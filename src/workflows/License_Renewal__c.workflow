<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Previous_Licence_Cap</fullName>
        <description>Updates the previous licence cap</description>
        <field>Previous_License_Cap__c</field>
        <formula>Asset__r.License_Cap__c</formula>
        <name>Previous Licence Cap</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Previous_Licence_End_Date_Update</fullName>
        <field>Previous_Licence_End_Date__c</field>
        <formula>Asset__r.UsageEndDate</formula>
        <name>Previous Licence End Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Previous_Licence_Start_Date_Update</fullName>
        <field>Previous_Licence_Start_Date__c</field>
        <formula>Asset__r.InstallDate</formula>
        <name>Previous Licence Start Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Previous License Cap Update</fullName>
        <actions>
            <name>Previous_Licence_Cap</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Previous_Licence_End_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Previous_Licence_Start_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>License_Renewal__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Recording of the previous license cap at renewal time</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
