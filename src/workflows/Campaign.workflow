<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify_AFRICA_Finance_User_on_Campaign_Creation</fullName>
        <ccEmails>accountsza@3plearning.com</ccEmails>
        <description>Notify AFRICA Finance User on Campaign Creation</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>ACTNSCRM__SF_NetSuite/Campaign_Email_Teamplate_for_Finance</template>
    </alerts>
    <alerts>
        <fullName>Notify_APAC_Finance_User_on_Campaign_Creation</fullName>
        <ccEmails>finance@3plearning.com</ccEmails>
        <description>Notify APAC Finance User on Campaign Creation</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>ACTNSCRM__SF_NetSuite/Campaign_Email_Teamplate_for_Finance</template>
    </alerts>
    <alerts>
        <fullName>Notify_CANADA_Finance_User_on_Campaign_Creation</fullName>
        <ccEmails>invoice@3plearning.com</ccEmails>
        <description>Notify CANADA Finance User on Campaign Creation</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>ACTNSCRM__SF_NetSuite/Campaign_Email_Teamplate_for_Finance</template>
    </alerts>
    <alerts>
        <fullName>Notify_EME_Finance_User_on_Campaign_Creation</fullName>
        <ccEmails>ukteamfinance@3plearning.com</ccEmails>
        <description>Notify EME Finance User on Campaign Creation</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>ACTNSCRM__SF_NetSuite/Campaign_Email_Teamplate_for_Finance</template>
    </alerts>
    <alerts>
        <fullName>Notify_NZ_Finance_User_on_Campaign_Creation</fullName>
        <ccEmails>accountsnz@3plearning.com</ccEmails>
        <description>Notify NZ Finance User on Campaign Creation</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>ACTNSCRM__SF_NetSuite/Campaign_Email_Teamplate_for_Finance</template>
    </alerts>
    <alerts>
        <fullName>Notify_US_Finance_User_on_Campaign_Creation</fullName>
        <ccEmails>accounts.usa@3plearning.com</ccEmails>
        <description>Notify US Finance User on Campaign Creation</description>
        <protected>false</protected>
        <recipients>
            <recipient>gideon.madlang@3plearning.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ACTNSCRM__SF_NetSuite/Campaign_Email_Teamplate_for_Finance</template>
    </alerts>
    <alerts>
        <fullName>Sending_an_email_to_Marketing_Department_on_Job_Code_Updation</fullName>
        <description>Sending an email to Marketing Department on Job Code Updation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ACTNSCRM__SF_NetSuite/Campaign_Email_Teamplate_for_Marketing_after_Job_Code_added</template>
    </alerts>
    <rules>
        <fullName>Notify marketing about Job Code</fullName>
        <actions>
            <name>Sending_an_email_to_Marketing_Department_on_Job_Code_Updation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notifying marketing that job code has been set by the Finance</description>
        <formula>OR(ISCHANGED(Job_Code__c), ISCHANGED(Job_Code_Internal_id__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
