<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Change_Priority_to_Low</fullName>
        <field>Priority</field>
        <literalValue>Low</literalValue>
        <name>Change Priority to Low</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Priority_to_Medium</fullName>
        <description>Medium</description>
        <field>Priority</field>
        <literalValue>Medium</literalValue>
        <name>Change Priority to Medium</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Priority_to_High</fullName>
        <description>This changes the priority of the case to High if the chosen category identifies the priority as High</description>
        <field>Priority</field>
        <literalValue>High</literalValue>
        <name>Priority to High</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ServiceCloud-Update Priority to High</fullName>
        <actions>
            <name>Priority_to_High</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Category__c</field>
            <operation>contains</operation>
            <value>High</value>
        </criteriaItems>
        <description>This workflow updates the priority of the case based on the chosen category to High.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ServiceCloud-Update Priority to Low</fullName>
        <actions>
            <name>Change_Priority_to_Low</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Category__c</field>
            <operation>contains</operation>
            <value>Low</value>
        </criteriaItems>
        <description>This workflow updates the priority of the case based on the chosen category to Low.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ServiceCloud-Update Priority to Medium</fullName>
        <actions>
            <name>Change_Priority_to_Medium</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Category__c</field>
            <operation>contains</operation>
            <value>Medium</value>
        </criteriaItems>
        <description>This workflow updates the priority of the case based on the chosen category to Medium.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
