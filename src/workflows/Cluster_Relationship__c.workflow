<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Total</fullName>
        <field>Total_Number_of_Students__c</field>
        <formula>School__r.Total_Number_Students__c</formula>
        <name>Set Total</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_total_students</fullName>
        <field>Total_Number_of_Students__c</field>
        <formula>School__r.Total_Number_Students__c</formula>
        <name>Set total students</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Unique_Relationship</fullName>
        <description>Sets value of Cluster__r.Id &amp; &quot;-&quot; &amp; School__r.Id</description>
        <field>Unique_Relationship__c</field>
        <formula>Account__r.Id &amp; &quot;-&quot; &amp; School__r.Id</formula>
        <name>Update Unique Relationship</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Students on Relationship</fullName>
        <actions>
            <name>Set_total_students</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Cluster_Relationship__c.CreatedById</field>
            <operation>notEqual</operation>
            <value>X</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Unique Relationship ID - Cluster Relationship</fullName>
        <actions>
            <name>Update_Unique_Relationship</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Cluster_Relationship__c.Relationship_Type__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Sets an ID for the cluster relationship, intent is to prevent duplicate relationships between schools and clusters</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
