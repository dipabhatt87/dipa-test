<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_notification_of_Quote_approval</fullName>
        <description>Email notification of Quote approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Quote_Approval_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_notification_of_Quote_rejection</fullName>
        <description>Email notification of Quote rejection</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Quote_Rejection_notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Record_type_update</fullName>
        <description>Update the record type to Approved Quote</description>
        <field>RecordTypeId</field>
        <lookupValue>Approved_Quote</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record type update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sent_for_Approval_set_to_False</fullName>
        <description>Checkbox Sent for Approval set to False after rejection</description>
        <field>Sent_for_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Sent for Approval set to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_Approved</fullName>
        <description>Quote Status changed to Approved</description>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_Rejected</fullName>
        <description>Change status to rejected</description>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submitted_for_Approval_Update</fullName>
        <field>Sent_for_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Submitted for Approval Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
