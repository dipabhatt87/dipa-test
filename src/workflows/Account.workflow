<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ACTNSCRM__Notify_Account_Owner_for_Credit_Hold</fullName>
        <description>Notify Account Owner for Credit Hold</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ACTNSCRM__SF_NetSuite/ACTNSCRM__Account_is_on_Credit_Hold</template>
    </alerts>
    <alerts>
        <fullName>Account_Owner_Notification</fullName>
        <description>Account Owner Notification</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/At_Risk_Email</template>
    </alerts>
    <alerts>
        <fullName>At_Risk_Email_Notification</fullName>
        <description>At Risk Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>alexandre.marson@3plearning.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/At_Risk_Email</template>
    </alerts>
    <alerts>
        <fullName>At_Risk_Email_Notification_Asia</fullName>
        <description>At Risk Email Notification Asia</description>
        <protected>false</protected>
        <recipients>
            <recipient>kiyomi.ryter@3plearning.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>smccallum@salesforce.3p</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/At_Risk_Email</template>
    </alerts>
    <alerts>
        <fullName>At_Risk_Email_Notification_EME</fullName>
        <description>At Risk Email Notification EME</description>
        <protected>false</protected>
        <recipients>
            <recipient>bronwen.jones@3plearning.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>charmagne.rayman-bacchus@3plearning.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/At_Risk_Email</template>
    </alerts>
    <alerts>
        <fullName>At_Risk_Email_Notification_NZ</fullName>
        <description>At Risk Email Notification NZ</description>
        <protected>false</protected>
        <recipients>
            <recipient>karen.rolleston@3plearning.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/At_Risk_Email</template>
    </alerts>
    <alerts>
        <fullName>At_Risk_Email_Notification_SA</fullName>
        <description>At Risk Email Notification SA</description>
        <protected>false</protected>
        <recipients>
            <recipient>rob.masefield@3plearning.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/At_Risk_Email</template>
    </alerts>
    <alerts>
        <fullName>At_Risk_Email_Notification_UK</fullName>
        <description>At Risk Email Notification UK</description>
        <protected>false</protected>
        <recipients>
            <recipient>bronwen.jones@3plearning.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>charmagne.rayman-bacchus@3plearning.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/At_Risk_Email</template>
    </alerts>
    <alerts>
        <fullName>At_Risk_Email_Notification_US</fullName>
        <description>At Risk Email Notification US</description>
        <protected>false</protected>
        <recipients>
            <recipient>alexander.dalrymple@3plearning.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/At_Risk_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Nullify_Into_Followup_Date</fullName>
        <field>Risk_Follow_up_Date_IntoScience__c</field>
        <name>Nullify Into Followup Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Nullify_Math_Risk_Followup_Date</fullName>
        <field>Risk_Follow_up_Date_math__c</field>
        <name>Nullify Math Risk Followup Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Nullify_Reading_Eggs_Followup_Date</fullName>
        <field>Risk_Follow_up_Date_Reading_Eggs__c</field>
        <name>Nullify Reading Eggs Followup Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Nullify_Spello_Risk_Followup_Date</fullName>
        <field>Risk_Follow_up_Date_Spell__c</field>
        <name>Nullify Spello Risk Followup Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Shipping_Address</fullName>
        <field>ShippingStreet</field>
        <formula>BillingStreet</formula>
        <name>Set Shipping Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Shipping_Country</fullName>
        <field>ShippingCountry</field>
        <formula>BillingCountry</formula>
        <name>Set Shipping Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Shipping_State</fullName>
        <field>ShippingState</field>
        <formula>BillingState</formula>
        <name>Set Shipping State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Shipping_Zipcode</fullName>
        <field>ShippingPostalCode</field>
        <formula>BillingPostalCode</formula>
        <name>Set Shipping Zipcode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Site_Latitude</fullName>
        <description>Populates Latitude from shipping (site) latitude</description>
        <field>Site_Coordinates__Latitude__s</field>
        <formula>ShippingLatitude</formula>
        <name>Site Latitude</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Site_Longitude</fullName>
        <description>populates the longitude from the Site (shipping) longitude</description>
        <field>Site_Coordinates__Longitude__s</field>
        <formula>ShippingLongitude</formula>
        <name>Site Longitude</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ACTNSCRM__Notify Account Owner for Credit Hold</fullName>
        <actions>
            <name>ACTNSCRM__Notify_Account_Owner_for_Credit_Hold</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.ACTNSCRM__Credit_Hold__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>At Risk Notification Asia</fullName>
        <actions>
            <name>Account_Owner_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>At_Risk_Email_Notification_Asia</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(   	OR 	(  		( 			AND( 			 				NOT(INCLUDES(PRIORVALUE(Risk_Indicator__c),&quot;COMP: Considering competitor&quot;)),				 				NOT(ISBLANK(Risk_Indicator__c)),		 				INCLUDES(Risk_Indicator__c, &quot;COMP: Considering competitor&quot;) 				) 		), 		 		( 			AND( 				NOT(INCLUDES(PRIORVALUE(Risk_Indicator_IntoScience__c),&quot;COMP: Considering competitor&quot;)),  				NOT(ISBLANK(Risk_Indicator_IntoScience__c)),		 				INCLUDES(Risk_Indicator_IntoScience__c, &quot;COMP: Considering competitor&quot;) 				) 		), 		 		( 			AND( 				NOT(INCLUDES(PRIORVALUE(Risk_Indicator_Spellodrome__c),&quot;COMP: Considering competitor&quot;)),  				NOT(ISBLANK(Risk_Indicator_Spellodrome__c)),		 				INCLUDES(Risk_Indicator_Spellodrome__c, &quot;COMP: Considering competitor&quot;) 				) 		), 		 		( 			AND( 				NOT(INCLUDES(PRIORVALUE(Risk_Indicator_Reading_Eggs__c),&quot;COMP: Considering competitor&quot;)),   				NOT(ISBLANK(Risk_Indicator_Reading_Eggs__c)),		 				INCLUDES(Risk_Indicator_Reading_Eggs__c, &quot;COMP: Considering competitor&quot;) 				) 		) 	), 	  	Customer_Avg_Value__c &gt; 25000,   	OR( TEXT(ShippingCountryCode)=&quot;AU&quot;,  	TEXT(ShippingCountryCode)=&quot;HK&quot;,  	TEXT(ShippingCountryCode)=&quot;CN&quot;,  	TEXT(ShippingCountryCode)=&quot;MY&quot;,  	TEXT(ShippingCountryCode)=&quot;PK&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>At Risk Notification CA</fullName>
        <actions>
            <name>Account_Owner_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>At_Risk_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends email to account owner when school is at risk</description>
        <formula>AND(  	OR 	(  		( 			AND( 			 				NOT(INCLUDES(PRIORVALUE(Risk_Indicator__c),&quot;COMP: Considering competitor&quot;)),				 				NOT(ISBLANK(Risk_Indicator__c)),		 				INCLUDES(Risk_Indicator__c, &quot;COMP: Considering competitor&quot;) 				) 		), 		 		( 			AND( 				NOT(INCLUDES(PRIORVALUE(Risk_Indicator_IntoScience__c),&quot;COMP: Considering competitor&quot;)),  				NOT(ISBLANK(Risk_Indicator_IntoScience__c)),		 				INCLUDES(Risk_Indicator_IntoScience__c, &quot;COMP: Considering competitor&quot;) 				) 		), 		 		( 			AND( 				NOT(INCLUDES(PRIORVALUE(Risk_Indicator_Spellodrome__c),&quot;COMP: Considering competitor&quot;)),  				NOT(ISBLANK(Risk_Indicator_Spellodrome__c)),		 				INCLUDES(Risk_Indicator_Spellodrome__c, &quot;COMP: Considering competitor&quot;) 				) 		), 		 		( 			AND( 				NOT(INCLUDES(PRIORVALUE(Risk_Indicator_Reading_Eggs__c),&quot;COMP: Considering competitor&quot;)),   				NOT(ISBLANK(Risk_Indicator_Reading_Eggs__c)),		 				INCLUDES(Risk_Indicator_Reading_Eggs__c, &quot;COMP: Considering competitor&quot;) 				) 		) 	), 	TEXT(ShippingCountryCode)=&quot;CA&quot;, Customer_Avg_Value__c &gt; 15000  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>At Risk Notification EME</fullName>
        <actions>
            <name>Account_Owner_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>At_Risk_Email_Notification_EME</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(  	OR 	(  		( 			AND( 			 				NOT(INCLUDES(PRIORVALUE(Risk_Indicator__c),&quot;COMP: Considering competitor&quot;)),				 				NOT(ISBLANK(Risk_Indicator__c)),		 				INCLUDES(Risk_Indicator__c, &quot;COMP: Considering competitor&quot;) 				) 		), 		 		( 			AND( 				NOT(INCLUDES(PRIORVALUE(Risk_Indicator_IntoScience__c),&quot;COMP: Considering competitor&quot;)),  				NOT(ISBLANK(Risk_Indicator_IntoScience__c)),		 				INCLUDES(Risk_Indicator_IntoScience__c, &quot;COMP: Considering competitor&quot;) 				) 		), 		 		( 			AND( 				NOT(INCLUDES(PRIORVALUE(Risk_Indicator_Spellodrome__c),&quot;COMP: Considering competitor&quot;)),  				NOT(ISBLANK(Risk_Indicator_Spellodrome__c)),		 				INCLUDES(Risk_Indicator_Spellodrome__c, &quot;COMP: Considering competitor&quot;) 				) 		), 		 		( 			AND( 				NOT(INCLUDES(PRIORVALUE(Risk_Indicator_Reading_Eggs__c),&quot;COMP: Considering competitor&quot;)),   				NOT(ISBLANK(Risk_Indicator_Reading_Eggs__c)),		 				INCLUDES(Risk_Indicator_Reading_Eggs__c, &quot;COMP: Considering competitor&quot;) 				) 		) 	), 	Customer_Avg_Value__c &gt; 20000,  	OR( TEXT(ShippingCountryCode)=&quot;TR&quot;, TEXT(ShippingCountryCode)=&quot;BH&quot;, TEXT(ShippingCountryCode)=&quot;KW&quot;,  	TEXT(ShippingCountryCode)=&quot;MA&quot;, TEXT(ShippingCountryCode)=&quot;OM&quot;, TEXT(ShippingCountryCode)=&quot;QA&quot;,  	TEXT(ShippingCountryCode)=&quot;SA&quot;, TEXT(ShippingCountryCode)=&quot;AE&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>At Risk Notification NZ</fullName>
        <actions>
            <name>Account_Owner_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>At_Risk_Email_Notification_NZ</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(  OR 	(  		( 			AND( 			 				NOT(INCLUDES(PRIORVALUE(Risk_Indicator__c),&quot;COMP: Considering competitor&quot;)),				 				NOT(ISBLANK(Risk_Indicator__c)),		 				INCLUDES(Risk_Indicator__c, &quot;COMP: Considering competitor&quot;) 				) 		), 		 		( 			AND( 				NOT(INCLUDES(PRIORVALUE(Risk_Indicator_IntoScience__c),&quot;COMP: Considering competitor&quot;)),  				NOT(ISBLANK(Risk_Indicator_IntoScience__c)),		 				INCLUDES(Risk_Indicator_IntoScience__c, &quot;COMP: Considering competitor&quot;) 				) 		), 		 		( 			AND( 				NOT(INCLUDES(PRIORVALUE(Risk_Indicator_Spellodrome__c),&quot;COMP: Considering competitor&quot;)),  				NOT(ISBLANK(Risk_Indicator_Spellodrome__c)),		 				INCLUDES(Risk_Indicator_Spellodrome__c, &quot;COMP: Considering competitor&quot;) 				) 		), 		 		( 			AND( 				NOT(INCLUDES(PRIORVALUE(Risk_Indicator_Reading_Eggs__c),&quot;COMP: Considering competitor&quot;)),   				NOT(ISBLANK(Risk_Indicator_Reading_Eggs__c)),		 				INCLUDES(Risk_Indicator_Reading_Eggs__c, &quot;COMP: Considering competitor&quot;) 				) 		) 	),  TEXT(ShippingCountryCode)=&quot;NZ&quot;, Customer_Avg_Value__c &gt; 10000  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>At Risk Notification SA</fullName>
        <actions>
            <name>Account_Owner_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>At_Risk_Email_Notification_SA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(  OR 	(  		( 			AND( 			 				NOT(INCLUDES(PRIORVALUE(Risk_Indicator__c),&quot;COMP: Considering competitor&quot;)),				 				NOT(ISBLANK(Risk_Indicator__c)),		 				INCLUDES(Risk_Indicator__c, &quot;COMP: Considering competitor&quot;) 				) 		), 		 		( 			AND( 				NOT(INCLUDES(PRIORVALUE(Risk_Indicator_IntoScience__c),&quot;COMP: Considering competitor&quot;)),  				NOT(ISBLANK(Risk_Indicator_IntoScience__c)),		 				INCLUDES(Risk_Indicator_IntoScience__c, &quot;COMP: Considering competitor&quot;) 				) 		), 		 		( 			AND( 				NOT(INCLUDES(PRIORVALUE(Risk_Indicator_Spellodrome__c),&quot;COMP: Considering competitor&quot;)),  				NOT(ISBLANK(Risk_Indicator_Spellodrome__c)),		 				INCLUDES(Risk_Indicator_Spellodrome__c, &quot;COMP: Considering competitor&quot;) 				) 		), 		 		( 			AND( 				NOT(INCLUDES(PRIORVALUE(Risk_Indicator_Reading_Eggs__c),&quot;COMP: Considering competitor&quot;)),   				NOT(ISBLANK(Risk_Indicator_Reading_Eggs__c)),		 				INCLUDES(Risk_Indicator_Reading_Eggs__c, &quot;COMP: Considering competitor&quot;) 				) 		) 	),  TEXT(ShippingCountryCode)=&quot;ZA&quot;, Customer_Avg_Value__c &gt; 150000  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>At Risk Notification UK</fullName>
        <actions>
            <name>Account_Owner_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>At_Risk_Email_Notification_UK</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(  OR 	(  		( 			AND( 			 				NOT(INCLUDES(PRIORVALUE(Risk_Indicator__c),&quot;COMP: Considering competitor&quot;)),				 				NOT(ISBLANK(Risk_Indicator__c)),		 				INCLUDES(Risk_Indicator__c, &quot;COMP: Considering competitor&quot;) 				) 		), 		 		( 			AND( 				NOT(INCLUDES(PRIORVALUE(Risk_Indicator_IntoScience__c),&quot;COMP: Considering competitor&quot;)),  				NOT(ISBLANK(Risk_Indicator_IntoScience__c)),		 				INCLUDES(Risk_Indicator_IntoScience__c, &quot;COMP: Considering competitor&quot;) 				) 		), 		 		( 			AND( 				NOT(INCLUDES(PRIORVALUE(Risk_Indicator_Spellodrome__c),&quot;COMP: Considering competitor&quot;)),  				NOT(ISBLANK(Risk_Indicator_Spellodrome__c)),		 				INCLUDES(Risk_Indicator_Spellodrome__c, &quot;COMP: Considering competitor&quot;) 				) 		), 		 		( 			AND( 				NOT(INCLUDES(PRIORVALUE(Risk_Indicator_Reading_Eggs__c),&quot;COMP: Considering competitor&quot;)),   				NOT(ISBLANK(Risk_Indicator_Reading_Eggs__c)),		 				INCLUDES(Risk_Indicator_Reading_Eggs__c, &quot;COMP: Considering competitor&quot;) 				) 		) 	),  Customer_Avg_Value__c &gt; 10000, TEXT(ShippingCountryCode)=&quot;GB&quot;  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>At Risk Notification US</fullName>
        <actions>
            <name>Account_Owner_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>At_Risk_Email_Notification_US</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(  OR 	(  		( 			AND( 			 				NOT(INCLUDES(PRIORVALUE(Risk_Indicator__c),&quot;COMP: Considering competitor&quot;)),				 				NOT(ISBLANK(Risk_Indicator__c)),		 				INCLUDES(Risk_Indicator__c, &quot;COMP: Considering competitor&quot;) 				) 		), 		 		( 			AND( 				NOT(INCLUDES(PRIORVALUE(Risk_Indicator_IntoScience__c),&quot;COMP: Considering competitor&quot;)),  				NOT(ISBLANK(Risk_Indicator_IntoScience__c)),		 				INCLUDES(Risk_Indicator_IntoScience__c, &quot;COMP: Considering competitor&quot;) 				) 		), 		 		( 			AND( 				NOT(INCLUDES(PRIORVALUE(Risk_Indicator_Spellodrome__c),&quot;COMP: Considering competitor&quot;)),  				NOT(ISBLANK(Risk_Indicator_Spellodrome__c)),		 				INCLUDES(Risk_Indicator_Spellodrome__c, &quot;COMP: Considering competitor&quot;) 				) 		), 		 		( 			AND( 				NOT(INCLUDES(PRIORVALUE(Risk_Indicator_Reading_Eggs__c),&quot;COMP: Considering competitor&quot;)),   				NOT(ISBLANK(Risk_Indicator_Reading_Eggs__c)),		 				INCLUDES(Risk_Indicator_Reading_Eggs__c, &quot;COMP: Considering competitor&quot;) 				) 		) 	), 	Customer_Avg_Value__c &gt; 15000,  	OR( TEXT(ShippingCountryCode)=&quot;US&quot;,  	TEXT(ShippingCountryCode)=&quot;BZ&quot;, TEXT(ShippingCountryCode)=&quot;CR&quot;,  	TEXT(ShippingCountryCode)=&quot;SV&quot;, TEXT(ShippingCountryCode)=&quot;GT&quot;,  	TEXT(ShippingCountryCode)=&quot;HN&quot;, TEXT(ShippingCountryCode)=&quot;MX&quot;,  	TEXT(ShippingCountryCode)=&quot;NI&quot;, TEXT(ShippingCountryCode)=&quot;PA&quot;,  	TEXT(ShippingCountryCode)=&quot;AR&quot;, TEXT(ShippingCountryCode)=&quot;BO&quot;,  	TEXT(ShippingCountryCode)=&quot;BR&quot;, TEXT(ShippingCountryCode)=&quot;CL&quot;,  	TEXT(ShippingCountryCode)=&quot;CO&quot;, TEXT(ShippingCountryCode)=&quot;EC&quot;,  	TEXT(ShippingCountryCode)=&quot;GF&quot;, TEXT(ShippingCountryCode)=&quot;GY&quot;,  	TEXT(ShippingCountryCode)=&quot;PY&quot;, TEXT(ShippingCountryCode)=&quot;PE&quot;,  	TEXT(ShippingCountryCode)=&quot;SR&quot;, TEXT(ShippingCountryCode)=&quot;UY&quot;,  	TEXT(ShippingCountryCode)=&quot;VE&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Nullify Risk Followup Date - Into Science</fullName>
        <actions>
            <name>Nullify_Into_Followup_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Risk_Follow_up_Date_IntoScience__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Risk_Indicator_IntoScience__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Nullify Risk Followup Date - Math</fullName>
        <actions>
            <name>Nullify_Math_Risk_Followup_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Risk_Follow_up_Date_math__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Risk_Indicator__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Nullify Risk Followup Date - ReadingEggs</fullName>
        <actions>
            <name>Nullify_Reading_Eggs_Followup_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Risk_Follow_up_Date_Reading_Eggs__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Risk_Indicator_Reading_Eggs__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Nullify Risk Followup Date - Spello</fullName>
        <actions>
            <name>Nullify_Spello_Risk_Followup_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Risk_Follow_up_Date_Spell__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Risk_Indicator_Spellodrome__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Risk Indicator Into Task Reminder</fullName>
        <actions>
            <name>Followup_Risk_Indicated_for_Into_Science</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Account.Risk_Follow_up_Date_IntoScience__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Create a follow-up task for the Risks Indicated for the Schools</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Risk Indicator Math Task Reminder</fullName>
        <actions>
            <name>Followup_Risk_Indicated_for_Mathletics</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Account.Risk_Follow_up_Date_math__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Create a follow-up task for the Risks Indicated for the Schools</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Risk Indicator Reading Eggs Task Reminder</fullName>
        <actions>
            <name>Followup_Risk_Indicated_for_Reading_Eggs</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Account.Risk_Follow_up_Date_Reading_Eggs__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Create a follow-up task for the Risks Indicated for the Schools</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Risk Indicator Spellodrome Task Reminder</fullName>
        <actions>
            <name>Followup_Risk_Indicated_for_Spellodrome</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Account.Risk_Follow_up_Date_Spell__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Create a follow-up task for the Risks Indicated for the Schools</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Followup_Risk_Indicated_for_Into_Science</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Account.Risk_Follow_up_Date_IntoScience__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Followup Risk Indicated for IntoScience</subject>
    </tasks>
    <tasks>
        <fullName>Followup_Risk_Indicated_for_Mathletics</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Account.Risk_Follow_up_Date_math__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Followup Risk Indicated for Mathletics</subject>
    </tasks>
    <tasks>
        <fullName>Followup_Risk_Indicated_for_Reading_Eggs</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Account.Risk_Follow_up_Date_Reading_Eggs__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Followup Risk Indicated for Reading Eggs</subject>
    </tasks>
    <tasks>
        <fullName>Followup_Risk_Indicated_for_Spellodrome</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Account.Risk_Follow_up_Date_Spell__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Followup Risk Indicated for Spellodrome</subject>
    </tasks>
</Workflow>
