<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Unique_Value</fullName>
        <field>Unique_Relationship__c</field>
        <formula>Teacher__r.Id &amp; &quot;-&quot; &amp;  Affiliate__r.Id</formula>
        <name>Set Unique Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Unique Value</fullName>
        <actions>
            <name>Set_Unique_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Affil__c.Affiliation_Status__c</field>
            <operation>equals</operation>
            <value>Member,Former Member</value>
        </criteriaItems>
        <description>set a field value of Account Id - Contact Id to ensure uniqueness of relationship</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
