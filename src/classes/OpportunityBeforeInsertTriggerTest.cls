@IsTest
public class OpportunityBeforeInsertTriggerTest {
    private static id clusterAccountRecodTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cluster').getRecordTypeId();
    private static id standardOpportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Standard Opportunity').getRecordTypeId();   
    private static id clusterAccountOpportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Cluster Opportunity').getRecordTypeId();   

    static Opportunity testOpportunity1;
    static Opportunity testOpportunity2;
    static Account testAccount1;
    static Account testAccount2;
    
    @testSetup static void setup()
    {
        FakeObjectFactory.testclasssetup();
        testAccount1  = FakeObjectFactory.GetClusterAccount();        
        insert testAccount1; 
        testOpportunity1 = FakeObjectFactory.GetStandardOpportunity();
        testOpportunity1.Type = 'Renewal';    
        testOpportunity1.name = 'opportunity_1';
        TestUtility.AssignOpportunityToAccount(testOpportunity1, testAccount1);
        insert testOpportunity1;
        
        testAccount2  = FakeObjectFactory.GetSchoolAccount();        
        insert testAccount2; 
        testOpportunity2 = FakeObjectFactory.GetStandardOpportunity();
        testOpportunity2.Type = 'Renewal';    
        testOpportunity2.name = 'opportunity_2';
        TestUtility.AssignOpportunityToAccount(testOpportunity2, testAccount2);
        insert testOpportunity2;
    }
    
    @IsTest(seeAllData = false)
    static void testClusterOpportunityIsCreatedWhenAccountIsCluster(){
        Opportunity oppo2 = [Select id , RecordTypeId from Opportunity where name = 'opportunity_1' limit 1];
        System.assert(oppo2.RecordTypeId == clusterAccountOpportunityRecordTypeId);  
    }
    
    @IsTest(seeAllData = false)
    static void testStandardOpportunityIsCreatedWhenAccountIsStandard(){
        Opportunity oppo2 = [Select id , RecordTypeId from Opportunity where name = 'opportunity_2' limit 1];
        System.assert(oppo2.RecordTypeId == standardOpportunityRecordTypeId);  
    }
}