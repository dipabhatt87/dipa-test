@isTest
private class SchoolPriceService_Test
{

   
    public static String accountid = TestUtility.getFakeId(Account.SObjectType);
    public static String numberoflicences =  '100';
    public static String campaignid = TestUtility.getFakeId(Campaign.SObjectType);
    public static String productfamily = 'Mathletics';
    public static String term = '3';
    @testSetup static void setup()
    {
        FakeObjectFactory.testclasssetup();
    }

	@isTest
	static void testValidationForSchoolServiceAccountParameter()
	{   	 	
    	accountid = null;
        RestRequest req =  getRequestParameters();
    	
    	RestResponse res = new RestResponse();

    	RestContext.request = req;
    	RestContext.response = res;

    	Test.startTest();

    	 SchoolPriceService.Get();
    	Test.stopTest();
    	System.debug('Response body' + res.responseBody.toString());
    	System.assertEquals(res.responseBody.toString(), 'AccountId needs to be provided');
    	System.assertEquals(res.statusCode, 400);
    	//System.assert(..., ..., '...');
	}

    @isTest
    static void testValidationForSchoolServiceCorrectAccountIdParameter()
    {           
        accountid = TestUtility.getFakeId(Campaign.SObjectType);
        RestRequest req =  getRequestParameters();
        
        RestResponse res = new RestResponse();

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();

         SchoolPriceService.Get();
        Test.stopTest();
        System.debug('Response body' + res.responseBody.toString());
        System.assertEquals(res.responseBody.toString(), 'Account Id is not valid');
        System.assertEquals(res.statusCode, 400);
        //System.assert(..., ..., '...');
    }



    @isTest
    static void testValidationForSchoolServicenumberOfStudentsParameter()
    {   
         accountid  = TestUtility.getFakeId(Account.SObjectType);              
        numberoflicences = null;
        RestRequest req =  getRequestParameters();
        
        RestResponse res = new RestResponse();

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();

         SchoolPriceService.Get();
        Test.stopTest();
       // System.debug('Response body' + res.responseBody.toString());
        System.assertEquals(res.responseBody.toString(), 'Number of Students needs to be provided');
        System.assertEquals(res.statusCode, 400);
        //System.assert(..., ..., '...');
    }

    @isTest
    static void testValidationForSchoolServicenumberOfStudentsNumericGreaterThanParameter()
    {           
        numberoflicences = '0';
        RestRequest req =  getRequestParameters();
        
        RestResponse res = new RestResponse();

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();

         SchoolPriceService.Get();
        Test.stopTest();
       // System.debug('Response body' + res.responseBody.toString());
        System.assertEquals(res.responseBody.toString(), 'Number of Students must be greater than zero');
        System.assertEquals(res.statusCode, 400);
        //System.assert(..., ..., '...');
    }



      @isTest
    static void testValidationForSchoolServicenumberOfProductFamilyParameter()
    {           
        productfamily = null;
        RestRequest req =  getRequestParameters();
        
        RestResponse res = new RestResponse();

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();

         SchoolPriceService.Get();
        Test.stopTest();
       // System.debug('Response body' + res.responseBody.toString());
        System.assertEquals(res.responseBody.toString(), 'Product Family needs to be provided');
        System.assertEquals(res.statusCode, 400);
        //System.assert(..., ..., '...');
    }

  @isTest
    static void testValidationForSchoolServiceTermParameter()
    {           
        term = null;
        RestRequest req =  getRequestParameters();
        
        RestResponse res = new RestResponse();

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();

         SchoolPriceService.Get();
        Test.stopTest();
        //System.debug('Response body' + res.responseBody.toString());
        System.assertEquals(res.responseBody.toString(), 'Term needs to be provided');
        System.assertEquals(res.statusCode, 400);
        //System.assert(..., ..., '...');
    }

   


    @isTest
    static void testValidationForSchoolServiceCorrectCampaignIdParameter()
    {           
        campaignid = TestUtility.getFakeId(Account.SObjectType);
        RestRequest req =  getRequestParameters();
        
        RestResponse res = new RestResponse();

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();

         SchoolPriceService.Get();
        Test.stopTest();
        //System.debug('Response body' + res.responseBody.toString());
        System.assertEquals(res.responseBody.toString(), 'CampaignId is not valid');
        System.assertEquals(res.statusCode, 400);
        //System.assert(..., ..., '...');
    }

    // @isTest
    // static void testValidationForSchoolPriceService()
    // {           
    //      Account account  = FakeObjectFactory.GetSchoolAccount();
      
    //     insert account; 
    //     campaignid = null;
    //     accountid = account.Id;
    //     RestRequest req =  getRequestParameters();
        
    //     RestResponse res = new RestResponse();

    //     RestContext.request = req;
    //     RestContext.response = res;

    //     Test.startTest();

    //     SchoolPriceService.Get();
    //     Test.stopTest();
   
    //     System.assertEquals(400, res.statusCode);
   
    // }

    @isTest
    static void testValidationForSchoolFromDifferentTerritory()
    {           
         Account account  = FakeObjectFactory.GetSchoolAccount();
        account.ShippingState       = 'Alberta';
        account.ShippingCountry     = 'Canada';
        account.ShippingPostalCode  = '8041';
        account.Territory__c = 'Canada';
        //insert account; 
        campaignid = null;
        accountid = account.Id;
        RestRequest req =  getRequestParameters();
        
        RestResponse res = new RestResponse();

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();

         SchoolPriceService.Get();
        Test.stopTest();
        //System.debug('Response body' + res.responseBody.toString());
        //System.assertEquals(res.responseBody.toString(), 'CampaignId is not valid');
        System.assertEquals(400, res.statusCode);
        //System.assert(..., ..., '...');   //System.assert(..., ..., '...');
    }

    @isTest
    static void testSuccessForSchool()
    {           
         Account account  = FakeObjectFactory.GetSchoolAccount();
        // account.ShippingState       = 'New South Wales';
        // account.ShippingCountry     = 'Australia';
        // account.ShippingPostalCode  = '2000';
         account.School_Decile__c = '800';
        // account.Territory__c = 'APAC';
        insert account; 
        String productFamily = 'Mathletics'; 
        Product2 product = getProduct(productFamily,'154');
        insert product;

        system.debug('Account Id:'+account.id );

        ACTNSPC__Currency__c currencyToInsert = getcurrency('AUD');
        insert currencyToInsert;
        

        ACTNSPC__Price_Level__c priceLevel = getPriceLevel('Decile','1');
        insert priceLevel;
        
        ACTNSPC__Price_Level_Entry__c  priceLevelEntry = getPriceLevelEntry(product,priceLevel, currencyToInsert,'154','Decile');
        insert priceLevelEntry;
        system.debug('priceLevelEntry Id:'+priceLevelEntry.id );
        
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry standardPrice = new PricebookEntry(
        Pricebook2Id = pricebookId, Product2Id = product.Id,
        UnitPrice = 4, IsActive = true);
        insert standardPrice;

        List<Pricebook__mdt> pricebooks = [select Id, name__c from Pricebook__mdt where name__c like '%decile%'];
        
        Pricebook2 pricebookNew = new Pricebook2(Name=pricebooks[0].name__c, isActive=true);
        insert pricebookNew;

        ACTNSPC__Price_Group__c pricegroup = new  ACTNSPC__Price_Group__c();
        pricegroup.ACTNSPC__External_Id__c = '452';
        insert pricegroup;

        ACTNSPC__Price_Book_Configuration__c configure = new ACTNSPC__Price_Book_Configuration__c();
        configure.ACTNSPC__Price_Book__c = pricebookNew.Id;
        configure.ACTNSPC__Price_Level__c = priceLevel.Id;
        configure.ACTNSPC__Automation_Id__c = '324';
        configure.ACTNSPC__Price_Group__c = pricegroup.Id;
        insert configure;           
            
        
        RegionTaxSetting__c regionTaxMap = new RegionTaxSetting__c();
        regionTaxMap.name = 'APAC-Australia';
        regionTaxMap.TaxRate__c = 10;
        insert regionTaxMap;

        PricebookEntry pricebookEntryNew = new PricebookEntry(
        Pricebook2Id = pricebookNew.Id, Product2Id = product.Id,
        UnitPrice = 4, IsActive = true);
        insert pricebookEntryNew;

        
        Campaign discountCampaign = new Campaign();
        discountCampaign.name = 'Discount';
        discountCampaign.Discount__c = 10;
        insert discountCampaign;

        //To test MCServiceHelperclass 
            List<PricebookEntry> priceBookEntry =  MCServiceHelper.getPriceBookEntry(productfamily,'AUD',account) ;    

        campaignid = discountCampaign.Id;
        accountid = account.Id;
        RestRequest req =  getRequestParameters();
         system.debug('Request Parameters:'+req);
        RestResponse res = new RestResponse();

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();

            SchoolPriceService.Get();
        Test.stopTest();
        //System.debug('Response body' + res.responseBody.toString());
        //System.assertEquals(res.responseBody.toString(), 'CampaignId is not valid');
        System.assertEquals(200, res.statusCode );
        //System.assert(..., ..., '...');
    }

    @isTest
    static void testActiveLicenceForSchool()
    {           
         Account account  = FakeObjectFactory.GetSchoolAccount();
        account.ShippingState       = 'New South Wales';
        account.ShippingCountry     = 'Australia';
        account.ShippingPostalCode  = '2000';
        insert account; 
        String productFamily = 'Mathletics'; 
        Product2 product = getProduct(productFamily,'154');
        insert product;
        Asset asset = FakeObjectFactory.GetFullAsset(product, System.today().addDays(-2), System.today().addMonths(6));
        asset.accountId = account.Id;
        System.debug('Anydatatype_msg' + asset);
        insert asset;
        campaignid = null;
        accountid = account.Id;
        RestRequest req =  getRequestParameters();
        
        RestResponse res = new RestResponse();

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();

         SchoolPriceService.Get();
        Test.stopTest();
        //System.debug('Response body' + res.responseBody.toString());
        //System.assertEquals(res.responseBody.toString(), 'CampaignId is not valid');
        System.assertEquals(200, res.statusCode);
        //System.assertEquals(false, res.ispriceavailable);
        //System.assert(..., ..., '...');
    }

    



    public static RestRequest getRequestParameters(){
        RestRequest req = new RestRequest();
        req.params.put('accountid', accountid);
        req.params.put('numberoflicences', numberoflicences);
        req.params.put('campaignid', campaignid);
        req.params.put('productfamily', productfamily);
        req.params.put('term', term);
        system.debug('getRequestParameters:'+ req);
        return req;
    }

      public static ACTNSPC__Currency__c getcurrency(String currencyCode){
            ACTNSPC__Currency__c currencyAUD    = new ACTNSPC__Currency__c();
            currencyAUD.ACTNSPC__Active__c      = true;
            currencyAUD.ACTNSPC__Iso_Code__c    = currencyCode;
            currencyAUD.Name                    = currencyCode;
            return currencyAUD;

      }

      public static ACTNSPC__Price_Level__c getPriceLevel(String name,String internalId){
            ACTNSPC__Price_Level__c priceLevel2 = new ACTNSPC__Price_Level__c();
            priceLevel2.Name = name;
            priceLevel2.ACTNSPC__External_Id__c = internalId;
            return priceLevel2;

      }    

     public static Product2 getProduct(string productFamily,String internalId){
             
            Product2 productWithTierprice = FakeObjectFactory.GetProduct(productFamily);
            productWithTierprice.ACTNSPC__Quantity_Tier_2__c = 75;
            productWithTierprice.ACTNSPC__Quantity_Tier_3__c = 150;
            productWithTierprice.ACTNSPC__Quantity_Tier_4__c = 300;
            productWithTierprice.ACTNSPC__Quantity_Tier_5__c = 500;
            productWithTierprice.Quantity_Tier_6__c = 1000;
            productWithTierprice.Quantity_Tier_7__c = 3000;
           
            productWithTierprice.ACTNSCRM__NetSuite_Internal_ID__c =internalId;
            productWithTierprice.ACTNSCRM__NS_Internal_ID__c = internalId;
            return productWithTierprice;

      }

      //This is  to get pricelevelEntry
      public static ACTNSPC__Price_Level_Entry__c getPriceLevelEntry(Product2 productWithTierprice,ACTNSPC__Price_Level__c priceLevel2,ACTNSPC__Currency__c currencyToInsert,String internalId, String name){
            

             
             //priceLevel2.ACTNSPC__Netsuite_Item_Id__c = '545';
             ACTNSPC__Price_Level_Entry__c  priceLevel = new ACTNSPC__Price_Level_Entry__c();
             priceLevel.ACTNSPC__Tier_1_Price__c = 2.80000000;
             priceLevel.ACTNSPC__Tier_2_Price__c = 2.38000000;
             priceLevel.ACTNSPC__Tier_3_Price__c = 2.24000000;
             priceLevel.ACTNSPC__Tier_4_Price__c = 1.40000000;
             priceLevel.ACTNSPC__Tier_5_Price__c = 1.40000000;
             priceLevel.ACTNSPC__Tier_6_Price__c = 1.26000000;
             priceLevel.Name                     = name;
            
             priceLevel.ACTNSPC__Currency__c = currencyToInsert.Id;
             priceLevel.ACTNSPC__Price_Level__c = priceLevel2.Id;
             priceLevel.ACTNSPC__External_Id__c = internalId;
             priceLevel.ACTNSPC__Netsuite_Item_Id__c = internalId;
             priceLevel.ACTNSPC__Product__c = productWithTierprice.Id;
             return priceLevel;
      }
}