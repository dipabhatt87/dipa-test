public with sharing class FullCalendarController {

    @AuraEnabled
    public static List<Webinar_Schedule__c> getEvents(){
        return [SELECT Name,Start_Time__c,End_Time__c,Id FROM Webinar_Schedule__c];
    }
}