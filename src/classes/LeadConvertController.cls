public with sharing class LeadConvertController extends PageControllerBase {

    public Lead leadConvert {get;set;}
    public Boolean isAccountPayble{get;set;}
    public DuplicateContactDetailsCompController myComponentController  {get;set;}
    public DuplicateAPContactDetailsCompController apContactController{get;set;}
    public leadConvertDetailsComponentController  leadDetailsComponentController{get;set;}
    public leadConvertTaskInfoComponentController leadTaskController{get;set;}
    private static String INACTIVE_CONTACT_OWNER = 'Selected contact has inactive owner. Please assign an active owner to related '+
    'contact record before converting the lead.';
    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public LeadConvertController(ApexPages.StandardController stdController) {
        this.leadConvert = (Lead)stdController.getRecord();
        leadConvert = [select Firstname,Lastname,Email,Country,Company,City,PostalCode,ConvertedAccountId,ConvertedOpportunityId,ConvertedContactId,
                 Account_Payable_Email__c, Account_Payable_First_Name__c,Job_Function__c, ownerId,Account_Payable_Last_Name__c, Purchase_Order_number__c,AccountID__c
              from Lead 
                  where Id= :leadConvert.Id];
        isAccountPayble = true;          
        System.debug('LeadConvertController Lead : ' +  leadConvert);
       leadTaskController = new leadConvertTaskInfoComponentController();
       

    }

      public override void setComponentController(ComponentControllerBase compController) {
        myComponentController = (DuplicateContactDetailsCompController)compController;
      }
        
      public override ComponentControllerBase getMyComponentController() {
        return myComponentController;
      }

       public override void setAPContactComponentController(ComponentControllerBase compController) {
         apContactController = (DuplicateAPContactDetailsCompController)compController;
      }
        
      public override ComponentControllerBase getAPContactComponentController() {
        return apContactController;
      }

      public override void setLeadDetailsComponentController(ComponentControllerBase compController) {
         leadDetailsComponentController = (leadConvertDetailsComponentController)compController;
      }
        
      public override ComponentControllerBase getLeadDetailsComponentController() {
        return leadDetailsComponentController;
      }

      public override void setLeadTaskInfoCompController(ComponentControllerBase compController) {
         leadTaskController = (leadConvertTaskInfoComponentController)compController;
      }
        
      public override ComponentControllerBase getLeadTaskInfoCompController() {
        return leadTaskController;
      }

   
    //This is an lead convert functionality
    public PageReference Submit(){
        Savepoint sp = Database.setSavepoint();
        try {
              if(myComponentController.selectedcontactId == null && (myComponentController.createNewContact == false || myComponentController.createNewContact == null) ){
                 
                  PrintError('Any record from contact list should be selected or create new contact checkbox should be checked');
                  return null;
              }else if((leadConvert.Account_Payable_Email__c != null && lead.Account_Payable_Last_Name__c != null) && apContactController.selectedAPcontactId == null && (apContactController.createNewAPContact == false || apContactController.createNewAPContact == null)){
                  PrintError('Any record from account payble contact list should be selected or create new account payble checkbox should be checked');
                  return null;
              }else{
                Account newAccount        = new Account();
                String oldAccountId;
                Database.LeadConvertResult leadConvertResult;
                Id accountsPayableContactId;

                  System.debug('SelecteDContactId before' +myComponentController.selectedcontactId );
                //Account insert
                if(leadDetailsComponentController.selectedAccount == 'NONE'){
                    PrintError('Please select an Account.');
                    return null;
                }else if(leadDetailsComponentController != NULL && leadDetailsComponentController.selectedAccount != 'NEW'){
                    newAccount.Id = leadDetailsComponentController.selectedAccount;
                }else if(leadDetailsComponentController != NULL && leadDetailsComponentController.selectedAccount == 'NEW'){
                     newAccount = new LeadConversionHelper().getAccountForLead(leadDetailsComponentController.selectedAccount,leadConvert);
                        insert newAccount;
                }
                
                
              String productFamily ;

              
              //Contact Insert
               Id contactId; 
                    Contact existingContact;
                    //Contact existingContact = [Select Id, AccountId,FirstName, LastName, Email,Job_Function__c from Contact where Id = :selectedcontactId];
                     System.debug('SelecteDContactId' +myComponentController.selectedcontactId );
                     if(myComponentController.selectedcontactId != null){
                      existingContact = new LeadConversionHelper().getContactForLead(myComponentController.selectedcontactId);
                      //check if the owner is inactive.
                      if(existingContact.ownerid!= null)
                      {
                        List<User> contactOwner = [select id,name, isactive from user where id = :existingContact.ownerid];
                        system.debug('Ayaz here '+contactOwner+' '+existingContact.ownerid + ' '+contactOwner.get(0).isActive);
                        if(!contactOwner.isEmpty() && !contactOwner.get(0).isActive )
                        {
                            PrintError(INACTIVE_CONTACT_OWNER);
                            return null;
                        }
                      }
                       oldAccountId = existingContact.AccountId;
                        System.debug('ConvertLea test '+ oldAccountId);
                       existingContact.AccountId = newAccount.Id;
                       Update existingContact;

                       contactId = new LeadConversionHelper().callToContactService(newAccount.Id, myComponentController.selectedcontactId, existingContact.FirstName, existingContact.LastName, existingContact.Job_Function__c, 'Subscription Coordinator', existingContact.Email, 'Mathletics');
                    }else{
                       contactId = new LeadConversionHelper().callToContactService(newAccount.Id, null, leadConvert.FirstName, leadConvert.LastName, 'Teacher', 'Subscription Coordinator', leadConvert.Email, 'Mathletics');
                    }


                    // Lead conversion result
                    System.debug('ConvertLea test '+ contactId);
                    leadConvertResult = new LeadConversionHelper().convertLead(newAccount.Id,leadDetailsComponentController.opportunityId.Name ,contactId,leadConvert,leadDetailsComponentController.sendOwnerEmail,leadDetailsComponentController.contactId.ownerId, 
                    leadDetailsComponentController.doNotCreateOppty);
                    System.debug('lead result '+ leadConvertResult);
                    if(leadConvertResult.isSuccess()){
                        
                        //Account Payble Insert
                        string accountPayblecontactId =  new LeadConversionHelper().getAccountPaybleToInsert(newAccount.Id,apContactController.selectedAPcontactId , 'Accounts', 'Accounts Payable',leadConvert,apContactController.createNewAPContact,getAPContacts());
                        if(accountPayblecontactId != null && !leadDetailsComponentController.doNotCreateOppty){
                          insert createOpportunityContact(leadConvertResult.getOpportunityId(),accountPayblecontactId,'Accounts Payable');
                        }

                        //update existing contact account to original account
                        if(oldAccountId != null){
                           System.debug('ConvertLea oldAccountId before '+ oldAccountId);
                            System.debug('ConvertLea existingContact before '+ existingContact);
                          existingContact.AccountId = oldAccountId;
                          System.debug('existingContact.AccountId  '+ existingContact.AccountId );
                          Update existingContact;
                        }

                        //insert Opportunity Contact 
                        if(!leadDetailsComponentController.doNotCreateOppty)    
                        {
                            insert createOpportunityContact(leadConvertResult.getOpportunityId(),leadConvertResult.getContactId(),'Opportunity Contact'); 
                        }        


                        //Get and insert Task
                          insert new LeadConversionHelper().getTaskToCreate(leadTaskController,leadConvertResult,leadDetailsComponentController.contactID.ownerId);
                        //return to Creted Opporutnity page
                        PageReference pageRef;
                        if(!leadDetailsComponentController.doNotCreateOppty) 
                        {
                            pageRef = new PageReference('/' + leadConvertResult.getOpportunityId());
                        }
                        else{
                             pageRef = new PageReference('/' + leadConvertResult.getContactId());
                        }
                        return pageRef;

                    }else{

                        PrintErrors(leadConvertResult.errors);                        
                        return null;

                    }    


                   
                









                    /*System.debug('myComponentController '+ myComponentController);
                    System.debug('myComponentController '+ apContactController);
                    System.debug('leadTaskController '+ leadTaskController);
                    System.debug('leadDetailsComponentController '+ leadDetailsComponentController);
                    System.debug('SelecteDContactId '+ myComponentController.selecteDContactId);
                    System.debug('SelecteDAPContactId ' +apContactController.selectedAPcontactId);
                    System.debug('selectedAccount ' +leadDetailsComponentController.selectedAccount);
                     System.debug('SelecteDAPContactId ' +leadTaskController.taskID.Status);
                     System.debug('SelecteDAPContactId ' +leadTaskController.taskId.ActivityDate);

                    PageReference page = new PageReference('/' + leadConvert.Id);
                    page.setRedirect(true);
                    return page;*/

              }
         }
       catch(exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.error,ex.getMessage()));
            ExceptionLogData.InsertExceptionlog(leadConvert.Id, LoggingLevel.ERROR, 'Lead Conversion', ex, true, false, null, null);
            Database.rollback(sp);
            return null;
        }     
        
    }

    Public PageReference Cancel(){
        PageReference page = new PageReference('/' + leadConvert.Id);
        page.setRedirect(true);
        return page;
     }


     // This will return Mathing contacts wrapper
    public list<MatchingContact> getAPContacts()
    {
            
        return new LeadDuplicateContactHelper().getMathcingContacts(leadConvert.Account_Payable_First_Name__c,leadConvert.Account_Payable_Last_Name__c,leadConvert.Account_Payable_Email__c);
    }



     //This will retun opportunity contact to insert
      public OpportunityContacts__c createOpportunityContact(Id opportunityId, Id contactId, String role)
     {
       system.debug('this is to test contact email ' + contactId);
        OpportunityContacts__c oppCont = new OpportunityContacts__c();
            oppCont.Opportunity__c = opportunityId;
            oppCont.Contact__c = contactId;
            oppCont.Role__c = role;

        return oppCont;
     }

    //this method will take database errors and print them to teh PageMessages 
     public void PrintErrors(Database.Error[] errors)
    {
        for(Database.Error error : errors)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, error.message);
            ApexPages.addMessage(msg);
        }
    }


    //This method will put an error into the PageMessages on the page
    public void PrintError(string error) {
        ApexPages.Message msg = new 
            ApexPages.Message(ApexPages.Severity.ERROR, error);
        ApexPages.addMessage(msg);
    } 
}