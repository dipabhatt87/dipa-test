global with sharing class UpdateOppsToSoldFromDocverifySchedular implements Schedulable {
	
	 global void execute(SchedulableContext sc) {
        String opportunityQuery = 'select Id, Is_Docverify_Signed_Or_Completed__c, StageName, Won_Lost_Reason__c from Opportunity where Is_Docverify_Signed_Or_Completed__c= true';
        Database.executeBatch(new StatelessBatchExecutor(new UpdateOppsToSoldDocverifySignedCompleted(),opportunityQuery,null),5);
    }
}