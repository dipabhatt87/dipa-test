public with sharing class LeadFactory {
    public ResponseModel status{get;set;} 
    public String leadException{get;set;}
    public LeadFactory () {
        status = new ResponseModel();
    }

    public String createLead( string company , integer numberoflicences, integer term,  double confirmedUnitPrice,double totalAmount ,string 
                         firstname, string lastname, string email, string job, string role,string countrycode, string postcode,  string productfamily , 
                         string street1, string street2, string suburb,String campaignId,string accPaybleFirstname,string accPayblelastname,string accPaybleEmail,string ponumber,string accountId,string enquiryType, 
                         string productid, string pricebookid,string purchaseid,string mobilephone){
         status = new ResponseModel();
        Lead singleLead = new Lead();
        //singleLead.name = name;
        singleLead.FirstName = firstname;
        singleLead.LastName = lastname;
        singleLead.Email = email;
        singleLead.Job_Function__c = job;
        singleLead.Account_Payable_First_Name__c = accPaybleFirstname;
        singleLead.Account_Payable_Last_Name__c = accPayblelastname;
        singleLead.Account_Payable_Email__c = accPaybleEmail;
        singleLead.Purchase_Order_number__c = ponumber;
        if(countrycode.length() == 2){
            singleLead.CountryCode = countrycode;
        }else{
            singleLead.CountryCode =   getIsocodeMapping(countrycode);
        }
        singleLead.PostalCode = postcode;
        singleLead.Street = street1+street2;
        singleLead.City = suburb;
        singleLead.Company = company;
        singleLead.No_of_licences__c = numberoflicences;
        singleLead.Term__c = term;
        singleLead.Confirmed_unit_price__c =  confirmedUnitPrice;
        singleLead.Total_Amount__c = totalAmount;
        singleLead.Product_family__c = productfamily;
        singleLead.AccountID__C = accountId;
        singleLead.Enquiry_Type__c = enquiryType;
        singleLead.ProductId__c = productid;
        singleLead.PricebookId__c = pricebookid;
        singleLead.Marketing_Cloud_Purchase_Id__c = purchaseid;
        singleLead.MobilePhone = mobilephone;

        
        AssignmentRule AR = new AssignmentRule();
        AR = [select id from AssignmentRule where SobjectType = 'Lead' and Active = true limit 1];
        
        //Creating the DMLOptions for "Assign using active assignment rules" checkbox
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.assignmentRuleId= AR.id;

       
        //Setting the DMLOption on lead instance
        singleLead.setOptions(dmlOpts);
       

		if(enquiryType == 'Purchase')                             
        {
            singleLead.Reference__c = 'P-M-';
        }
        else if (enquiryType == 'Quote')
        {
            singleLead.Reference__c = 'Q-M-';
        }
        
        //singleLead.Campaign.ID = campaignId;
        if(role != null){
            singleLead.Contact_Role__c =role;
        }else{
            singleLead.Contact_Role__c = 'Subscription Coordinator';
        }
        if(productfamily =='Mathletics'){
            singleLead.Interest_Mathletics__c=true;
        }


        try{
            insert singleLead;
            if(isValidSalesforceId(campaignId,Campaign.class) && singleLead.Id != null  ){
                insert getCampaignMember(Id.valueOf(campaignId),singleLead.Id);
            }
        }catch(Exception ex){
            System.debug('This is catch' + ex.getMessage());
           
             leadException = ex.getMessage();
        }
        
        //System.debug('This is catch' + status.statusCode  + 'This is to sdbbshs' + status.statusMessage + 'this is to testlnhjkfa' + singleLead);
        If(singleLead.Id != null){
            return singleLead.Id;// 
        }else{
            return null;
        }



    }

  


    /**
     * Test a String to see if it is a valid SFDC  ID
     * @param  sfdcId The ID to test.
     * @param  t      The Type of the sObject to compare against
     * @return        Returns true if the ID is valid, false if it is not.
     */
    private Boolean isValidSalesforceId( String sfdcId, System.Type t ){
        try {
 
            if ( Pattern.compile( '[a-zA-Z0-9]{15}|[a-zA-Z0-9]{18}' ).matcher( sfdcId ).matches() ){
                // Try to assign it to an Id before checking the type
                Id id = sfdcId;
 
                // Use the Type to construct an instance of this sObject
                sObject sObj = (sObject) t.newInstance();
      
                // Set the ID of the new object to the value to test
                sObj.Id = id;
 
                // If the tests passed, it's valid
                return true;
            }
        } catch ( Exception e ){
            // StringException, TypeException
        }
 
        // ID is not valid
        return false;
    }

     private CampaignMember getCampaignMember(Id campaignId, Id LeadId){
                CampaignMember memb = new CampaignMember();
                    memb.CampaignId = campaignId;
                    memb.LeadId = LeadId;
                    memb.Status = 'Sent';
                    return memb;
     }

      private String getIsocodeMapping(String  countrycode){
        List<ISOMapping__c> isoMapping = [Select Id,ISO2__c,ISO3__c from ISOMapping__c where ISO3__c = :countrycode];
        if(isoMapping != null ){
             return isoMapping[0].ISO2__c;
        }else{
            return null;
        }       
       
     }

}