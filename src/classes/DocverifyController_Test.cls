/**
 * @author: Baskar Venugopal
 * @Description: Test class for Docverify Webhook controller.
 * @Test Class:    
 * @History: 
	09/08/2017 created.
 */
@isTest
public class DocverifyController_Test {

    public static testmethod void testDocverifyWebHook(){
       
        docverifyesign__DocVerify_Agreement__c eSign=new docverifyesign__DocVerify_Agreement__c();
        eSign.docverifyesign__Document_ID__c ='1234';
        eSign.Name='docName';
        eSign.docverifyesign__Status__c='viewed';
        insert eSign;
        
        PageReference webhook = Page.DocVerify_WebHook;
        Test.setCurrentPage(webhook);
        DocVerifyWebhook_Controller controller=new DocVerifyWebhook_Controller();
        
        ApexPages.currentPage().getParameters().put('docId', '1234');
		ApexPages.currentPage().getParameters().put('signer', 'xyz');
        ApexPages.currentPage().getParameters().put('status', 'signed');
        ApexPages.currentPage().getParameters().put('statusdate', string.valueOf(system.date.today()));
        
        controller.updateStatus();
        string  params=controller.parameterDetails;
 
    }
}