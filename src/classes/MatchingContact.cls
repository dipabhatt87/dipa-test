public with sharing class MatchingContact {

	public Contact con {get; set;}
	public Boolean isMatched {get; set;}

	public MatchingContact(Contact c) {
		this.con = c;
		isMatched = false;	
	}
}