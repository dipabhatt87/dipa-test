/**
* @author- Baskar Venugopal
* @Description : Test class for  UpdateOpportunityToLostWhenNoEsignature batch and UpdateOpportunityToLostSchedular.
* @Test Class:    
* @History: 
02/08/2017 created.
*/
@isTest
public class UpdateOpportunityToLost_Test {
    
    @testSetup static void setup()
    {
        FakeObjectFactory.testclasssetup(); 
        QuotesNotSigned__c settings =new QuotesNotSigned__c();
        settings.Docverify_Status__c ='Signature Completed';
        settings.Name='NoESignature';
        settings.Quote_Status__c ='Approved';
        settings.School_Classification__c ='Bronze,Silver';
        settings.Territory__c ='APAC,New Zealand';
        insert settings;
    } 
    
    
    public static testMethod void UpdateOpportunityToLostWhenNoEsignature_Test(){
        Account account  = FakeObjectFactory.GetSchoolAccount();
        account.School_Classification__c ='Bronze';
        insert account;
        
        datetime createdDateTime = datetime.now();
        createdDateTime.adddays(-30);
        Test.setCreatedDate(account.Id, createdDateTime); 
        
        Opportunity opportunity = FakeObjectFactory.GetStandardOpportunity();
        TestUtility.AssignOpportunityToAccount(opportunity, account);
        insert opportunity;
        
        Test.setCreatedDate(opportunity.Id, createdDateTime); 
        
        Quote quote=FakeObjectFactory.GetQuote();
        quote.OpportunityId=opportunity.id;
        insert quote;
        Test.setCreatedDate(quote.Id, createdDateTime);
        
        List<Opportunity> oppList=[select id,StageName,Won_Lost_Reason__c from Opportunity];
        system.debug('oppList##'+ oppList);
        UpdateOpportunityToLostWhenNoEsignature updateOpp=new UpdateOpportunityToLostWhenNoEsignature();                                   
 		
        updateOpp.execute(oppList);
        
        updateOpp.finish();
        
    }

    public static testMethod void UpdateOpportunityToLostSchedularTest(){
        
		Test.StartTest();
			UpdateOpportunityToLostSchedular schedular = new UpdateOpportunityToLostSchedular();
			String sch = '0 0 23 * * ?';
			system.schedule('Test UpdateOpportunity To Lost Schedular', sch, schedular);
		Test.stopTest();
	}
}