@RestResource(urlMapping='/school/purchase/*')
global class SchoolPurchaseService
{
    @httpPost 
    global static ResponseModel ConfirmPurchase(Id accountid, Id contactid, string productfamily,Id campaignid, integer numberoflicences, integer term, double unitprice, 
                                                double totalamount,
                                                string accpayblefirstname,string accpayblelastname,string accpaybleemail,string ponumber,string enquirytype, string productid, string pricebookid,String purchaseid)
    {
         RestResponse res = RestContext.response;

         string statusMessage;
         ResponseModel responseModel = new ResponseModel();
        if (res == null) {
            res = new RestResponse();
            RestContext.response = res;
              return null;
        }

        if(accountId==null ) 
        {
             res.statusCode = 400;           
             //return 'AccountId needs to be provided';
             //responseModel.objectID = ;
             //responseModel.referenceNumber = ;
             res.responseBody = Blob.valueOf('AccountId needs to be provided');
             //res.addHeader(String.valueOf(res.statusCode), 'AccountId needs to be provided');
             return null;
        }   

        if(contactId==null )
        {
             res.statusCode = 400;            
             res.responseBody = Blob.valueOf('ContactId needs to be provided');      
             return null;
        }

        if(numberoflicences<=0 )
        {
             res.statusCode = 400;           
             res.responseBody = Blob.valueOf('Number of students is required');       
             return null;
        }

        if(productFamily=='' || productFamily==null )
        {
             res.statusCode = 400;
             res.responseBody = Blob.valueOf('Product Family is required'); 
            return null;
        }

        if(unitprice <= 0 )
        {
             res.statusCode = 400;
            res.responseBody = Blob.valueOf('Confirmed Unit Price is required');
             return null;
        }

        if(totalAmount <= 0 )
        {
             res.statusCode = 400;            
             res.responseBody = Blob.valueOf('Total Amount is required');
             return null;
        }

        /*if(startDate <= System.today() )
        {
             res.statusCode = 400;
             return 'Start Date cannot be in past'; 
        }

        if(endDate <= startDate )
        {
             res.statusCode = 400;
             return 'End Date should be greater than startDate';    
        }

        if(unitprice*numberoflicences != totalAmount)
        {
             res.statusCode = 400;
            
             res.responseBody = Blob.valueOf('Total amount not matched provided number of students and confirmed unit price');
            return null;
        }
        */
        
       if(enquirytype == null)
        {
             res.statusCode = 400;
            
             res.responseBody = Blob.valueOf('Enquiry Type is required');
            return null;
        }        
        
        //Create helper Class
    Id opportunityId;
        try{
            if ( enquirytype == 'Purchase' || enquirytype == 'Quote')
            {
          opportunityId = GenerateQuoteAndEsignature.createOpportunity(productfamily, term,accountid,contactid,numberoflicences, 'buyingIntent', 
                                  ponumber,enquirytype,accpayblefirstname,accpayblelastname,accpaybleemail, decimal.valueOf(unitprice),campaignid,purchaseid);
                
            }
            else {
                res.statusCode = 400;            
                res.responseBody = Blob.valueOf('Enquiry Type can be only Purchase or Quote, please check the Enquiry Type');
                return null;
            }
        
            List<Opportunity> createdOpportunityList = [Select Id,OpportunityReferenceId__c from Opportunity where Id = :opportunityId];
            
            if(createdOpportunityList !=null && createdOpportunityList.size() > 0)
            {
                responseModel.objectID = createdOpportunityList[0].Id;//TestUtility.getFakeId(Opportunity.SObjectType);
                responseModel.referenceNumber = createdOpportunityList[0].OpportunityReferenceId__c;//'47'+TestUtility.getFakeId(Opportunity.SObjectType);             
            }
        }
        catch(Exception ex){
            system.debug('exception##'+ ex.getMessage());
            ExceptionLogData.InsertExceptionlog(opportunityId, LoggingLevel.ERROR, 'SchoolPurchaseService', ex, true, false, null, null);
        }
         return responseModel;
        //return responseModel;
        //return 'Congratulatoin! Purchase successfully done.';

        
    }
}