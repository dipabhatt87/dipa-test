@isTest
public class DocverifyAPI_Test {

    @isTest static void DocverifyAPITest(){
		DocverifyAPI api=new DocverifyAPI();
        DocverifyAPI.GetVersion_element element=new DocverifyAPI.GetVersion_element();
        DocverifyAPI.GetVersionResponse_element  respElement= new DocverifyAPI.GetVersionResponse_element();
        DocverifyAPI.AddNewDocumentESignFromTemplateResponse_element tempResp= new DocverifyAPI.AddNewDocumentESignFromTemplateResponse_element();
        DocverifyAPI.TestDocumentXMLData_element testXml= new DocverifyAPI.TestDocumentXMLData_element();
        DocverifyAPI.AddNewDocumentESignFromTemplate_element esignTempEle= new DocverifyAPI.AddNewDocumentESignFromTemplate_element();
        DocverifyAPI.AddNewDocumentESign_element esignEle= new DocverifyAPI.AddNewDocumentESign_element();
        DocverifyAPI.AddNewDocumentESignResponse_element esignResp_Ele=new DocverifyAPI.AddNewDocumentESignResponse_element();
        DocverifyAPI.GetSignatureDetailsJSONResponse_element getSign= new DocverifyAPI.GetSignatureDetailsJSONResponse_element();
        DocverifyAPI.UpdateWebHookURLResponse_element  updateResEle=new DocverifyAPI.UpdateWebHookURLResponse_element();
        DocverifyAPI.GetSignatureDetailsJSON_element jsonEle=new DocverifyAPI.GetSignatureDetailsJSON_element();
        DocverifyAPI.UpdateWebHookURL_element updateWebhook= new DocverifyAPI.UpdateWebHookURL_element();
        DocverifyAPI.DocVerify_x0020_Web_x0020_ServiceSoap soap=new DocverifyAPI.DocVerify_x0020_Web_x0020_ServiceSoap ();
        soap.AddNewDocumentESignFromTemplate(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,true,true,
                                             null,null,true,null,true,null);
        soap.UpdateWebHookURL(null,null,null);
        soap.TestDocumentXMLData(null,null,null,null) ;
        soap.GetVersion() ;
        soap.GetSignatureDetailsJSON(null,null,null,null,false); 
        soap.AddNewDocumentESign(null,null,null,1,null,null,null,null,null,null,null,null,true,true,null,true,null,true,null,null,false,null,true,null) ;
        
       	esignTempEle.apiKey ='test';
        esignTempEle.apiSig  ='test';
        esignTempEle.TemplateID  ='test';
        esignTempEle.creditType  =1;
        esignTempEle.DocumentName  ='test';
        esignTempEle.Description  ='test';
        esignTempEle.ClientID  ='test';
        esignTempEle.Emails  ='test';
        esignTempEle.MessageToSigners  ='test';
        esignTempEle.ReqOwnerESign=true;
        esignTempEle.OwnerESignFirst=true;
        esignTempEle.SecretWord  ='test';
        esignTempEle.RequireSecurityEnhanced=true;
        esignTempEle.RequestPhoneVerify= true;
        esignTempEle.XMLData  ='test';
        esignTempEle.SeparateSigners  =true;
        esignTempEle.AdditionalPDF  ='test';
        esignTempEle.OneSign=true;
        esignTempEle.AllowSignerElementAdd= true;
        esignTempEle.AdditionalImage  ='test';
        esignTempEle.UniqueID  ='test';
        esignTempEle.EnforceSigClick= false;
        esignTempEle.CustomSubject  ='test';
        esignTempEle.IDVerify= false;
        esignTempEle.CCEmails ='test@test.com';
    }
	
    @isTest static void DocverifyAsyncAPITest(){
        AsyncDocverifyAPI aysnc=new AsyncDocverifyAPI();
        AsyncDocverifyAPI.AddNewDocumentESignFromTemplateResponse_elementFuture addnewDoc=new AsyncDocverifyAPI.AddNewDocumentESignFromTemplateResponse_elementFuture();
        //addnewDoc.getValue();
        AsyncDocverifyAPI.UpdateWebHookURLResponse_elementFuture updaetWebhook=new AsyncDocverifyAPI.UpdateWebHookURLResponse_elementFuture();
        //updaetWebhook.getValue();
        AsyncDocverifyAPI.TestDocumentXMLDataResponse_elementFuture testDoc=new  AsyncDocverifyAPI.TestDocumentXMLDataResponse_elementFuture();
        //testDoc.getValue();
        AsyncDocverifyAPI.GetVersionResponse_elementFuture getVer=new AsyncDocverifyAPI.GetVersionResponse_elementFuture();
        //getVer.getValue();
        
        AsyncDocverifyAPI.GetSignatureDetailsJSONResponse_elementFuture getsign=new AsyncDocverifyAPI.GetSignatureDetailsJSONResponse_elementFuture();
        //getsign.getValue();
        
       AsyncDocverifyAPI.AsyncDocVerify_x0020_Web_x0020_ServiceSoap soap=new AsyncDocverifyAPI.AsyncDocVerify_x0020_Web_x0020_ServiceSoap();
        soap.inputHttpHeaders_x=new Map<String,String>();
        soap.inputHttpHeaders_x.put('test','test');
        
        soap.clientCertName_x='test';
        soap.timeout_x=1;
       soap.beginAddNewDocumentESignFromTemplate(null,null,null,null,1,null,null,null,null,null,false,false,null,false,false,null,false,null,false,true,null,null,false,null,false,null);
       soap.beginUpdateWebHookURL(null,null,null,null);
       soap.beginTestDocumentXMLData(null,null,null,null,null);
       soap.beginGetVersion(null);
       soap.beginGetSignatureDetailsJSON(null,null,null,null,true,true);
       soap.beginAddNewDocumentESign(null,null,null,null,1,null,null,null,null,null,true,true,null,true,true,null,true,null,true,null,null,false,null,false,null) ;

    }
}