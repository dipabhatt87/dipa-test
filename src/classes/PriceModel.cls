global class PriceModel
    {
        public Decimal TotalAmount {get; set;}
        public Decimal UnitPrice {get; set;}

        public Decimal Tax {get; set;}

        public String CurrencyCode {get; set;}

        public Decimal Subtotal {get; set;}

        public Decimal DicountedPriceIncludedTax {get; set;}

        public String taxname {get; set;}
        
        public Boolean ispriceavailable {get;set;}

        public String productid {get;set;}

        public String pricebookid {get;set;}

        public Decimal monthlyUnitPrice {get;set;}
    }