/**
* @author- Baskar Venugopal
* @Description This class schedular to schedul UpdateOpportunityToLostWhenNoEsignature batch.
* @Test Class: UpdateOpportunityToLost_Test   
* @History: 
01/08/2017 created.
*/
global class UpdateOpportunityToLostSchedular implements Schedulable{
    global void execute(SchedulableContext sc) {
        List<Id> oppIds=new List<Id>();
        date oneMonthPrior = system.Date.today().adddays(-31);
        //Retrieve values from Custom settings
        QuotesNotSigned__c settings = QuotesNotSigned__c.getValues('NoESignature'); 
        List<string> territories= settings.Territory__c.split(',');
        List<string> docStatus=settings.Docverify_Status__c.split(','); 
        List<string> quoteStatus=settings.Quote_Status__c.split(','); 
        List<string> schoolClassifications=settings.School_Classification__c.split(','); 
        try{
            for(List<Quote> quotesNotSigned :[Select OpportunityId from Quote  where 
                                              //Status='Approved' and 
                                              DAY_ONLY(Createddate) =: oneMonthPrior and 
                                              Account.School_Classification__c in : schoolClassifications and 
                                              Account.Territory__c in : territories and 
                                              id not in (Select docverifyesign__Quote__c from docverifyesign__DocVerify_Agreement__c where 
                                                         docverifyesign__Status__c in : docStatus and 
                                                         Createddate = LAST_N_DAYS:30 )]){
                                                             for(Quote quoteNoSign : quotesNotSigned){
                                                                 oppIds.add(quoteNoSign.OpportunityId);
                                                             }                                     
                                                             
                                                         }
            Id oppRecTypeId=Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Standard Opportunity').getRecordTypeId();
            String opportunityQuery = 'select id,StageName,Won_Lost_Reason__c from Opportunity where RecordTypeId=' + oppRecTypeId+' and Id in ' + oppIds;
            if(oppIds != null && oppIds.size() >0){
                Database.executeBatch(new StatelessBatchExecutor(new UpdateOpportunityToLostWhenNoEsignature(),opportunityQuery,null),50);
            }
        }
        catch(Exception ex){
            ExceptionLogData.InsertExceptionlog(null, LoggingLevel.ERROR, 'UpdateOpportunityToLostSchedular', ex, true, false, null, null);
        }
    }
}