public with sharing virtual class ComponentControllerBase {
	public ComponentControllerBase() {
		
	}
	public PageControllerBase pageController { get; 
    set {
      if (value != null) {
	pageController = value;
	pageController.setComponentController(this);
      }
    }
  }

  public PageControllerBase pageControllerForAp { get; 
    set {
      if (value != null) {
	pageControllerForAp = value;
	pageControllerForAp.setAPContactComponentController(this);
      }
    }
  }

   public PageControllerBase pageControllerLeadDetails { get; 
    set {
      if (value != null) {
	pageControllerLeadDetails = value;
	pageControllerLeadDetails.setLeadDetailsComponentController(this);
      }
    }
  }

  public PageControllerBase pageControllerLeadTaskInfo { get; 
    set {
      if (value != null) {
	pageControllerLeadTaskInfo = value;
	pageControllerLeadTaskInfo.setLeadTaskInfoCompController(this);
      }
    }
  }
}