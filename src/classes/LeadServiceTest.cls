@isTest
private class LeadServiceTest  {
    public LeadServiceTest() {
        
    }
    @testSetup static void setup()
    {
      FakeObjectFactory.testclasssetup();


    }
   @isTest 
  static void  providedatawithnullcountry_statuscodeshouldbe400() {

    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();


    RestContext.request = req;
    RestContext.response = res;  
         
    LeadFactory factory = new LeadFactory();
    ResponseModel statusModal = new ResponseModel(); 
    statusModal = LeadService.createLead('test',8,2,123,878,'firstname','lastname','abc@test.com','job',null,null,'657','Mathletics','street1','street2','suburb','campaign','accPayFName','accPaLName','abd@test.com','5454df','ihreui84748','enquiry',null,null,null,'123434');  
 
    //Lead lead = [Select Id,name from lead where Id =:statusModal.objectId ];
    System.assertEquals(statusModal, null);
    //System.assertEquals(400, LeadService.res.statusCode);

  }

  @isTest
  static void  providecorrectdataForleadService_ServiceshouldReturnSuccess() {

    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();

    req.requestURI = 'https://cs5.salesforce.com/services/apexrest/v.9/lead';  
    req.httpMethod = 'Post';
    RestContext.request = req;
    RestContext.response = res;
    LeadService service = new LeadService();
     
    LeadFactory factory = new LeadFactory();
    ResponseModel statusModal = new ResponseModel();
    statusModal = LeadService.statusModal;
   
      statusModal = LeadService.createLead('test',8,2,123,878,'firstname','lastname','abc@test.com','job',null,'AUS','657','Mathletics','street1','street2','suburb','campaign','accPayFName','accPaLName','abd@test.com','5454df','ihreui84748','Quote',null,null,null,'12154');  
   
      Lead lead = [Select Id,name from lead where Id =:statusModal.objectId ];
      System.assertEquals(lead.Id, statusModal.objectId);
      //System.assertEquals(200, service.res.statusCode);
    //System.assertEquals(400, LeadService.statusModal.statusCode);
    
   
    

  }

   @isTest
  static void  providecorrectdataForleadService_ServiceshouldReturnSuccessWithAllField() {
      Campaign discountCampaign = new Campaign();
      discountCampaign.name = 'Discount';
      discountCampaign.Discount__c = 10;
      insert discountCampaign;
    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();

    req.requestURI = 'https://cs5.salesforce.com/services/apexrest/v.9/lead';  
    req.httpMethod = 'Post';
    RestContext.request = req;
    RestContext.response = res;
    LeadService service = new LeadService();
     
    LeadFactory factory = new LeadFactory();
    ResponseModel statusModal = new ResponseModel();
    statusModal = LeadService.statusModal;
   
      statusModal = LeadService.createLead('test',8,2,123,878,'firstname','lastname','abc@test.com','job','Accounts Payable','AU','657','Mathletics','street1','street2','suburb',discountCampaign.Id,'accPayFName','accPaLName','abd@test.com','5454df','ihreui84748','Purchase',null,null,null,'12154');  
   
      Lead lead = [Select Id,name from lead where Id =:statusModal.objectId ];
      System.assertEquals(lead.Id, statusModal.objectId);
      //System.assertEquals(200, service.res.statusCode);
    //System.assertEquals(400, LeadService.statusModal.statusCode);
    
   
    

  }


    
}