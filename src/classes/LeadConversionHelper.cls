public class LeadConversionHelper {
  public LeadConversionHelper() {
    
  }

  //This method is to get account for lead conversion
  public Account getAccountForLead(String selectedAccountId, Lead lead){
    Account newAccount = new Account();
    if(selectedAccountId == 'New'){
      newAccount.Name         = lead.Company;
      newAccount.ShippingCountry    = lead.Country;
          newAccount.ShippingPostalCode   = lead.PostalCode;
    }else{
      newAccount.Id = selectedAccountId;
    }
    
    return newAccount;
  }

  //This method is to get existing Contact for lead conversion
  public Contact getContactForLead(string contactId){
    System.debug('SelectedContact Id' + contactId);
    return [Select Id, AccountId,FirstName, LastName, Email,Job_Function__c, ownerid from Contact where Id = :contactId];
  }

  //This method is to call Contact Service 
  public Id callToContactService(String accountId,string existingContactId,string firstname,string lastname,string jobFunction,string role,string email,string productFamily){
    Id contactId = ContactService.doPost(accountId, existingContactId, firstname, lastname, jobFunction, role, email, productFamily,null);
    return contactId;
  }
  
  //This method is to convert Lead
  public Database.LeadConvertResult convertLead(string accountId,string opportunityName,string contactId, Lead lead,Boolean sendEmailToOwner, Id recordOwnerId, 
  Boolean doNotCreateOppty )
  {
    
    Database.LeadConvert lc = new Database.LeadConvert();
    lc.setLeadId(lead.id);
    lc.setOwnerID(recordOwnerID);
    lc.setSendNotificationEmail(sendEmailToOwner);
    //Assign the opportunity name only if we are creating an apportunity.
    if(!doNotCreateOppty)
    {
        lc.setOpportunityName(opportunityName);       
    }
    else{
        lc.setDoNotCreateOpportunity(doNotCreateOppty);
    }
    lc.setAccountId(accountId);
    lc.setContactId(contactId);
    LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
    lc.setConvertedStatus(convertStatus.MasterLabel);
    Database.LeadConvertResult lcr = Database.convertLead(lc);
    return lcr;

  }

  //This method is to get AccountPayble contact for an Account and Opportunity
  public Id getAccountPaybleToInsert(string accountId,string selectedAPcontactId,string role,string jobFunction,Lead lead,Boolean createNewAPContact, list<MatchingContact> MatchingContacts){
    String accountsPayableContactId;
    if (lead.Account_Payable_Email__c != null &&  lead.Account_Payable_First_Name__c != null &&  lead.Account_Payable_Last_Name__c != null ){
       
      if (createNewAPContact == true || MatchingContacts.size() == 0)
      {       
        accountsPayableContactId = callToContactService(accountId,null,lead.Account_Payable_First_Name__c, lead.Account_Payable_Last_Name__c,'Accounts', 'Accounts Payable', lead.Account_Payable_Email__c, 'Mathletics');                    
      }
      else
      {
          Contact existingAPContact = getContactForLead(selectedAPcontactId);
          accountsPayableContactId =callToContactService(accountId,selectedAPcontactId,existingAPContact.FirstName, existingAPContact.LastName,'Accounts', 'Accounts Payable', existingAPContact.Email, 'Mathletics'); 
      }
      
    }
    return accountsPayableContactId;
  }

 
 //This method will return task which needs to be creted on opportunity
 public Task getTaskToCreate(leadConvertTaskInfoComponentController myTaskComponentController,Database.LeadConvertResult leadConvertResult,String OppownerId){
    Task taskToCreate = new Task();
   if(myTaskComponentController != NULL && myTaskComponentController.taskID.subject != null){
      //set whether there is a reminder
      taskToCreate.IsReminderSet = myTaskComponentController.remCon.taskID.IsReminderSet;


          //if the reminder is set, and the reminder's date is set
                if (taskToCreate.IsReminderSet 
                    && myTaskComponentController.remCon.taskID.ActivityDate != null) {
          
          //set the reminder time based on the reminder class's ActivityDate
          //The date and time in the reminder class is converted into a datetime by the convertToDatetime() method
                    taskToCreate.ReminderDateTime = 
                        convertToDatetime(
                            myTaskComponentController.remCon.taskID.ActivityDate,
                            myTaskComponentController.remCon.reminderTime
                        );
                    system.debug('taskToCreate.ReminderDateTime --> ' + taskToCreate.ReminderDateTime);
                    
                } 
                //set the whatId to the Opportunity Id            
                taskToCreate.WhatId = leadConvertResult.getOpportunityId();
                
                //set the whoId to the contact Id
                taskToCreate.WhoId = leadConvertResult.getContactId();
                
                //set the subject
                taskToCreate.Subject = myTaskComponentController.taskID.Subject;
                
                //set the status
                taskToCreate.Status = myTaskComponentController.taskID.Status;
                
                //set the activity date 
                taskToCreate.ActivityDate = myTaskComponentController.taskID.ActivityDate;
                
                //set the Priority 
                taskToCreate.Priority = myTaskComponentController.taskID.Priority;
                
                //Set OwnerId
                taskToCreate.OwnerId = OppownerId;
                
                //set the custom field Primary Resource (this is a custom field on the Task showing an example of adding custom fields to the page)
                //taskToCreate.Primary_Resource__c = myTaskComponentController.taskID.Primary_Resource__c;
                
                //set the Description field which comes from the leadConvertTaskDescComponent
                taskToCreate.Description =  myTaskComponentController.taskID.Description;
   }

   return taskToCreate;
 }

 // given a date and time, where time is a string this method will return a DateTime
 @TestVisible
    private DateTime convertToDatetime(Date d, string t) {
        String timeFormat = DateTimeUtility.LocaleToTimeFormatMap().get(UserInfo.getLocale());
        
        //if the local of the user uses AM/PM 
        if (timeFormat != null && timeFormat.endsWith('a')) {
          
          //split the time into 2 strings 1 time and 1 am r pm
            string [] reminderTime = t.split(' ');
            
            //split the time into hour and minute
            string hour = reminderTime[0].split(':')[0];
            string min = reminderTime[0].split(':')[1];
            
            //get the am or pm
            string AM_PM = reminderTime[1];
            
            //turn the hour into an integer
            integer hr = Integer.valueOf(hour);
            
            //if the am/pm part of the string is PM then add 12 hours
            if (AM_PM.equalsIgnoreCase('PM')) hr += 12;
            
            //return a new DateTime based on the above information
            return (
                DateTime.newInstance(
                    d, 
                    Time.newInstance(
                        hr, 
                        Integer.valueOf(min), 
                        0,
                        0
                    )
                )
            ); 
        }
        //If the user's local does not use AM/PM and uses 24 hour time
        else {
            
            //split the time by a : to get hour and minute
            string hour = t.split(':')[0];
            string min = t.split(':')[1];
            
            //turn the hour into an integer
            integer hr = Integer.valueOf(hour);
            
            //return a new DateTime based on the above information
            return (
                DateTime.newInstance(
                    d, 
                    Time.newInstance(
                        hr, 
                        Integer.valueOf(min), 
                        0,
                        0
                    )
                )
            ); 
        }
    }

}