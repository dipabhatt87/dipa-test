public with sharing class LeadDuplicateContactHelper {

	public List<MatchingContact> wrapperListToShow{get;set;}
	public LeadDuplicateContactHelper(List<MatchingContact> wrapperListToShow){
		this.wrapperListToShow = wrapperListToShow;

	}
	
	public Boolean hasExactMatchContacts{get;set;}
	public LeadDuplicateContactHelper() {
		hasExactMatchContacts = false;
	}

	public List<MatchingContact> getMathcingContacts(string firstNAme,string lastName,string email){
		wrapperListToShow = new List<MatchingContact>();
		         
        List<Contact> contactList = getContactList(firstNAme,lastName,email,'And',new List<Contact>());
		wrapperListToShow.addAll(getMatchingContactList('And',contactList));
		if(wrapperListToShow == null || wrapperListToShow.size() ==0){
				wrapperListToShow.addAll(getMatchingContactList('Or',getContactList(firstNAme,lastName,email,'Or',contactList)));		
		}
	
        return wrapperListToShow;
		
	}

	@TestVisible
	private List<Contact> getContactList(string firstNAme,string lastName,string email,String matchingCriteria,List<Contact>existingContacts){
		String query = 'select Id, Firstname, Lastname, Email, Account.Name ,Account.ShippingCountry ,Job_Function__c,AccountId , Name from contact where ';
		List<Contact> contactList = new List<Contact>();
	
		if(matchingCriteria  == 'And'){
			contactList = [select Id, Firstname, Lastname, Email, Account.Name ,Status__c,Account.Status__c,
										 MailingCountry,MailingState,MailingCity,Account.ShippingCountry ,Job_Function__c,AccountId , Name
								  from contact
								  where 
								  (Firstname = :firstNAme and
								  Lastname 	 = :lastName and
								  Email 	 = :email ) Order By Email DESC NULLS LAST]; //and  Status__c = 'Current'
		   if
		   (contactList.size() > 0){
		   	 hasExactMatchContacts = true;
		   }						  
		}else if(matchingCriteria == 'Or'){
			contactList = [select Id, Firstname, Lastname, Email, Account.Name ,Status__c,Account.Status__c,
										 MailingCountry,MailingState,MailingCity, Account.ShippingCountry,Job_Function__c,AccountId, Name
								  from contact
								  where 
								  (( Email 	= :email AND Email != null) OR 
								  (Firstname = :firstNAme and Lastname 	 = :lastName)) 
								  and Id not in :existingContacts Order By Email DESC NULLS LAST];//And Status__c = 'Current'
		}
		
		return contactList;
	}
    
    @TestVisible
	private List<MatchingContact> getMatchingContactList(String matchingCriteria,List<Contact> contactList){
	MatchingContact wrapper;
	List<MatchingContact> wrapperList = new List<MatchingContact>();
		for(Contact con : contactList){
				wrapper = new MatchingContact(con);
				wrapper.con = con;
				if(matchingCriteria == 'And'){
					wrapper.isMatched = true;
				}
				wrapperList.add(wrapper);
		}

		return wrapperList;
	}
}