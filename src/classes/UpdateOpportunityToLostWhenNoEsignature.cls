/**
 * @author: Baskar Venugopal
 * @Description: This class implament the StatelessBatchExecutor to update Opportunities to Lost when the Approved Quotes not received E-Signature within 30 days.
 * @Test Class:    
 * @History: 
	01/08/2017 created.
 */
public class UpdateOpportunityToLostWhenNoEsignature implements StatelessBatchExecutor.CustomBatchable{
    
    public void execute(List<Opportunity> scope){    
        updateOpportunities(scope);
    }

    public void finish(){}
    
    /**
	 * @author: Baskar Venugopal
	 * @Description: Method to update Opportunities Stage to Lost and reason to No Response.
	 * @Param : Opportunity List to update.	 
	 * @History: 
		01/08/2017 created.
	 */
    public void updateOpportunities(List<Opportunity> opportunityList){
       try{
           for(Opportunity oppTobeUpdated : opportunityList){
               oppTobeUpdated.StageName = 'Lost';
               oppTobeUpdated.Won_Lost_Reason__c ='No Response';
           }
		   update opportunityList;
       }catch(Exception ex){
           System.debug('Exception -' +ex);
           ExceptionLogData.InsertExceptionlog(null, LoggingLevel.ERROR, 'UpdateOpportunityToLostWhenNoEsignature', ex, true, false, null, null);
       }       
        
    }
}