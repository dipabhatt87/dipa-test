@RestResource(urlMapping='/contact/*')
global class ContactService {

	/*@httpGet
	global static String doGet()
	{
		return 'Hello from salesforce contact service yayyy Zi';
	}*/

	//It creates a new contact and link it to specific account
	//AccountId is mandatory
	@httpPost
	global static Id doPost(Id accountId,Id contactId, String firstname, string lastname, string job, string role, string email, string productfamily, string mobilephone ){
		try{
			RestResponse res = RestContext.response;

	        if (res == null) {
	            res = new RestResponse();
	            RestContext.response = res;
	        } 
	 
	        if(job	 == null || job=='')
			{
				res.statusCode = 400;
				res.responseBody=Blob.valueOf('job needs to be provided.');	
				return null;	
			}      
			if(role	 == null || role=='')
			{
				role = 'Opportunity Contact';
	 
				//res.statusCode = 400;
				//res.responseBody=Blob.valueOf('role needs to be provided.');	
				//return null;	
			}

			if(contactId == null){
			
				if(firstName == null || firstName=='')
				{
					res.statusCode = 400;
					res.responseBody=Blob.valueOf('firstName needs to be provided.');	 
					return null;	
				}

				if(lastName	 == null || lastName=='')
				{
					res.statusCode = 400;
					res.responseBody=Blob.valueOf('lastName needs to be provided.');	
					return null;	
				}

				if(email	 == null || email=='')
				{
					res.statusCode = 400;
					res.responseBody=Blob.valueOf('email needs to be provided.');	
					return null;	
				}
		    }

			//Check if accountId is valid
			//Check if there is any contact with that email address, firstname , last name
			//if exists then check if there is a relationship between account and contact, if not creates the relationship
			//if not exist then create a new contact and then create a relationship also
			List<Contact> contacts;
			if(contactId != null){
			   contacts = [select Id , Firstname, LastName, Email ,AccountId,Status__c,MobilePhone
													from 
														Contact 
													where
													Id = :contactId];
			}else{
			 	contacts = [select Id , Firstname, LastName, Email ,AccountId,Status__c,MobilePhone
								from 
									Contact 
								where
								Email 		= :email 		AND
								FirstName 	= :firstName 	AND
								lastName 	= :LastName ];
			}

			Contact contact;

			if(contacts!=null && contacts.size()>0)
			{
				contact = contacts[0];

				if(contact.MobilePhone == null || contact.MobilePhone=='' )			
				{
					contact.MobilePhone		= mobilephone;
					update contact;
				}

				
			}

			//Check if contact exists or not
			if(contact == null)
			{
				contact 				= new Contact();
				contact.FirstName 		= firstname;
				contact.LastName 		= lastName;
				contact.Email 			= email;
				contact.Job_Function__c	= job;
				contact.AccountId 		= accountId;
				contact.MobilePhone		= mobilephone;
				insert contact;

				Contact_Roles__c contactRole = new 	Contact_Roles__c();

				contactRole.Contact__c 		= contact.Id;
				contactRole.Account__c 		= contact.AccountId;
				
				contactRole.Role__c			= role;		
				

				if(productFamily == 'Mathletics')
				contactRole.MInfluencer__c  = true;

				if(productFamily == 'Spellodrome') 
				contactRole.SInfluencer__c  = true;
                
                contactRole.School_Territory__c =getAccountTerritory(accountId);
				contactRole.Job_Function__c = job;
				insert contactRole;


			}
			else {

				List<Contact_Roles__c> contactRoles = [select Id,MInfluencer__c,SInfluencer__c,Role__c from 
												Contact_Roles__c
												where
												Contact__c = :contact.Id AND
												account__C = :accountId] ;

				Contact_Roles__c contactRole;
				
				if(contactRoles!=null && contactRoles.size()>0)								
				{
					contactRole =contactRoles[0];
				}
				
				if(contactRole== null)
				{
					contactRole = new Contact_Roles__c();
					// a new contact role needs to be added
					contactRole.Contact__c 		= contact.Id;
					contactRole.Account__c 		= accountId;
					//contactRole.Job_Function__c	= job;
					contactRole.Role__c			= role;		
					

					if(productFamily == 'Mathletics')
					contactRole.MInfluencer__c  = true;

					if(productFamily == 'Spellodrome')
					contactRole.SInfluencer__c  = true;

					//If the sekected contact is Inactive then following code will updtae it to Current.	
					if(contact.Status__c != 'Current'){
						contact.Status__c = 'Current';
						update contact; 
					}
                    
                    contactRole.School_Territory__c =getAccountTerritory(accountId);
					contactRole.Job_Function__c = job;
					insert contactRole;	

				}
				else {

					boolean isUpdated=false;

					if(contactRole.Role__c != role){
						contactRole.Role__c = role;
						isUpdated=true;
					}

					if(contactRole.MInfluencer__c == false && productFamily == 'Mathletics')
					{
						contactRole.MInfluencer__c  = true;
						isUpdated=true;
					}

					if(contactRole.SInfluencer__c == false && productFamily == 'Spellodrome')
					{
						contactRole.SInfluencer__c  = true;
						isUpdated=true;
					}

					if(isUpdated)
					{
						update contactRole;
					}
				}


				
			}
			res.statusCode = 200;
			res.responseBody=Blob.valueOf(contact.Id);	
			return contact.Id;
			}catch(Exception ex){
				 system.debug('exception##'+ ex.getMessage());
	            ExceptionLogData.InsertExceptionlog('ContactService', LoggingLevel.ERROR, 'ContactService', ex, true, false, null, null);
	            return null;
			}
 		
	}

	
    private static string getAccountTerritory(id accountId){
        List<Account> accounts=[Select id,Territory__c from Account where id=:accountId];
        if(accounts != null && accounts.size()>0){
            return accounts[0].Territory__c;
        }
        return null;
    }

	
}