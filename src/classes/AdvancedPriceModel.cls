public class AdvancedPriceModel {
	
	public decimal InitialAmount {get; set;}
	public decimal InitialUnitPrice {get; set;}
	public decimal FinalAmountIncludeTax {get; set;}
	public decimal FinalAmountExcludeTax {get; set;}
	public decimal AnualUnitPrice {get; set;}
	public decimal MonthlyUnitPrice {get; set;}	
	public decimal TaxAmount {get; set;}
	public decimal UnitPriceExcludeTax {get;set;}
	public decimal UnitPriceIncludeTax {get;set;}
	public decimal TaxRate {get;set;}

	public List<DiscountedPriceModel> Discounts {get; set;}

	public void SetPrice(Decimal anualUnitPrice, Integer term, boolean isPaymentUpfront, integer quantity , integer licenceMonth )
	{
		system.debug('anualUnitPrice:'+anualUnitPrice +   ',term:'+term+ ',isPaymentUpfront:'+isPaymentUpfront+ ',quantity:'+quantity +',licenceMonth:' + licenceMonth);
	
		if(isPaymentUpfront == true )
		{
			MonthlyUnitPrice = (anualUnitPrice / 12/term).setScale(2);
		}
		else
		{	MonthlyUnitPrice = (anualUnitPrice / 12).setScale(2);

		}
	
		this.AnualUnitPrice   = (MonthlyUnitPrice * 12).setScale(2);
		 
		if(isPaymentUpfront == true )
		{
				FinalAmountExcludeTax = (MonthlyUnitPrice * quantity * licenceMonth * term).setScale(2);
		}
		else
		{		FinalAmountExcludeTax = (MonthlyUnitPrice * quantity * licenceMonth ).setScale(2);

		}	
 
		if(isPaymentUpfront == true && term > 0)
		{
			this.AnualUnitPrice =  (this.AnualUnitPrice / term).setScale(2);			
		}
		
	}


	public decimal discountAmount {get {
		decimal temp=0;
		
		if(Discounts == null || Discounts.size()==0)
		{
			return 0;
		}

		for(DiscountedPriceModel discountModel : Discounts)
		{	
			if(discountModel.isDiscountVisible)
			temp=temp+discountModel.discountAmount;
		}

		return temp.setScale(2);
	}}

	public decimal discountPercentage {get {
		decimal temp=0;
		
		if(Discounts == null || Discounts.size()==0)
		{
			return 0;
		}

		for(DiscountedPriceModel discountModel : Discounts)
		{	
			if(discountModel.isDiscountVisible)
			temp=temp+discountModel.discountPercentage;
		}

		return temp;
	}}


	public DiscountedPriceModel getDiscount(DiscountType discountType)
	{
		
		for(DiscountedPriceModel item : Discounts)
		{
			
			if(discountType == item.discountType)
			{
				
				return  item;
			}
		}
		return new DiscountedPriceModel();
	}
	public AdvancedPriceModel()
	{
		Discounts 	= new 	List<DiscountedPriceModel>();
 		InitialAmount = 0;
		InitialUnitPrice  = 0;
		FinalAmountIncludeTax  = 0;
		FinalAmountExcludeTax  = 0;
		AnualUnitPrice  = 0;
		MonthlyUnitPrice = 0;
		TaxAmount = 0;
		UnitPriceExcludeTax = 0;
		UnitPriceIncludeTax = 0;
		TaxRate = 0;

		
	}
}