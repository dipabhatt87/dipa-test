public with sharing class MCServicesValidator {
	public MCServicesValidator() {
		
	}

	//This method is to validate  School price service input parameters
	public String validateSchoolServiceInputParameters(String accountId, String numberOfStudents, String campaignIdParam, String productfamily,
													 String term){
			//RestResponse res = new RestResponse();
			
			if(isNullValue(productfamily) == null){
				//res.statusCode = 400;
           		return 'Product Family needs to be provided';//res.responseBody=Blob.valueOf('Product Family needs to be provided');   
			}


			if(isNullValue(accountId) == null){
				 //res.statusCode = 400;
           		 return 'AccountId needs to be provided';//res.responseBody=Blob.valueOf('Product Family needs to be provided');  
			}else if(isNullValue(accountId) != null && !FakeObjectFactory.isValidSalesforceId(accountId,Account.class)){
				return 'Account Id is not valid';			
			}

			if(isNullValue(numberOfStudents) == null){
				 //res.statusCode = 400;
           		 return 'Number of Students needs to be provided';//res.responseBody=Blob.valueOf('Product Family needs to be provided');  
			}else if(!numberOfStudents.isNumeric()  || integer.valueOf(numberOfStudents)== 0){
				//res.statusCode = 400;
             	return 'Number of Students must be greater than zero';//res.responseBody=Blob.valueOf('Number of Students must be greater than zero');
              // RestContext.response.body = 'Param: leadId is not a valid Id';
               
			}

			if(campaignIdParam !=null && !FakeObjectFactory.isValidSalesforceId(campaignIdParam,Campaign.class)){
				 //res.statusCode = 400;
             	return 'CampaignId is not valid';// res.responseBody=Blob.valueOf('CampaignId is not valid');
			}

			if(isNullValue(term) == null){
				// res.statusCode = 400;
           		return 'Term needs to be provided'; //res.responseBody=Blob.valueOf('Term needs to be provided');  
			}

			return null;

	}

	//This method is to validate  Territory price service input parameters 
	public String validateTerritoryServiceInputParameters(String numberOfStudents, String campaignIdParam,String productfamily,String country, String trritory, 
																String term){
		RestResponse res = new RestResponse();
		if(isNullValue(productfamily) == null){
				
           		return 'Product Family needs to be provided';   
			}

		if(isNullValue(numberOfStudents) == null){
			 return 'Number of Students needs to be provided';
		}else if(!numberOfStudents.isNumeric()  || integer.valueOf(numberOfStudents)== 0){
			return 'Number of Students must be greater than zero';
          // RestContext.response.body = 'Param: leadId is not a valid Id';
           
		}

		if(campaignIdParam !=null && !FakeObjectFactory.isValidSalesforceId(campaignIdParam,Campaign.class)){
			 return 'CampaignId is not valid';
		}

		if(isNullValue(country) == null){
			return 'Country must be provided & cannot be null';   
		}

		if(isNullValue(trritory) == null){
			return 'territoryName needs to be provided'; 
		}

		if(isNullValue(term) == null){
			return  'Term needs to be provided';   
		}


		return null;

	}	

	public Boolean checkIfAccountHasActiveLicence(Account selectedAccount, String productfamily){
		List<Asset> assetList = [Select Id,name,  Status,UsageEndDate,InstallDate From Asset where UsageEndDate > Today And InstallDate < Today And Status = 'Provisioned' And product2.Family = :productfamily And  AccountId = :selectedAccount.Id];
        
		system.debug('assetList:'+ assetList.size());
		if(assetList != null && assetList.size() > 0){
        	return true;
        }
        return false;
	}

	public Boolean checkIfAccountisNotFromCorrectTerritory(Account selectedAccount){
		
		  List<Marketing_Cloud_Default_Pricebook__mdt> pricebookListForMC = [Select Pricebook__r.Name__c,Product_family__r.Name__c,Country__r.Code__c, 
                                                                                  Territory__r.Name__c  from Marketing_Cloud_Default_Pricebook__mdt
                                                                                  Where  
                                                                                  (Country__r.Code__c = :selectedAccount.ShippingCountryCode OR Country__r.Code__c  = 'All') And                   
                                                                                  Territory__r.Name__c = :selectedAccount.Territory__c];

			system.debug('pricebookListForMC:'+ pricebookListForMC.size());
			if(pricebookListForMC.size() == 0){
				return	true;
			}
			return false;
	}



	//This method is to check null value
	public String isNullValue(String parameter){
		return parameter != null ? parameter :null;
	}
}