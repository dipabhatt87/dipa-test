public with sharing class DuplicateAPContactDetailsCompController extends ComponentControllerBase{
	
	 public LeadDuplicateContactHelper helper{get;set;}

   // variable to store selectedApContact from List of matching accountpayble contacts 
    public string selectedAPcontactId{get; set;}
    
    //Variable to store value from checkbox to create new contact.
    public Boolean createNewAPContact{get; set;}
	  
   
   //This variable is to store lead which needs to convert 
    public Lead leadConvert{get;set;}

   //This variable is to tell you that whether lead account payble details or not 
    public Boolean isAccountPayble {get;set;}
    //THis is set by the <apex:attribute> and is the lead to convert
    /*public Lead leadConvert {
        get; 
        set { 
                leadConvert = value;
            }
        }
    public Boolean isAccountPayble {
        get; 
        set { 
                isAccountPayble = value;
            }
        }*/
    public DuplicateAPContactDetailsCompController(){
      //leadConvert = value;
      //isAccountPayble = value;
      System.debug('Anydatatype_msg 1' + leadConvert);
      // getContacts();
       //paginator = new ContactCustomPagination(matchingContactList);
    }

    

    
    public List<MatchingContact> getContacts()
  {
    helper = new LeadDuplicateContactHelper();
    if(isAccountPayble == true){
      System.debug('Account payble contacts ' + isAccountPayble);
       return helper.getMathcingContacts(leadConvert.Account_Payable_First_Name__c,leadConvert.Account_Payable_Last_Name__c,leadConvert.Account_Payable_Email__c);  
    }else {
        System.debug('Anydatatype_msg ' + leadConvert);
        return  helper.getMathcingContacts(leadConvert.firstName,leadConvert.lastName,leadConvert.email);  
    }
    
  }

public PageReference getAPSelected(){
      selectedAPcontactId = ApexPages.currentPage().getParameters().get('APcontactId');         
      createNewAPContact   = false;  
      return null;
  }

 
  

       /* public Boolean hasNext {
            get 
            {
               return null;// return paginator.hasNext();
            }
            set;
        }
        
        public Boolean hasPrevious {
            get 
            {
               return null;/// return paginator.hasPrevious();
            }
            set;
        }
        
        public void next() 
        {
            //matchingContactListNextPrevious = paginator.next();
        }
        
        public void previous() 
        {
           //matchingContactListNextPrevious = paginator.previous();
        }*/
  
}