@RestResource(urlMapping='/lead/*')
global class LeadService {

    public static ResponseModel statusModal{get;set;}
    @httppost
    global static ResponseModel CreateLead(string company , integer numberoflicences, integer term,  double confirmedunitprice,double totalamount ,string 
                         firstname, string lastname, string email, string job, string role,string countrycode, string postcode,  string productfamily , 
                         string street1, string street2, string suburb,String campaignid,string accpayblefirstname,string accpayblelastname,string accpaybleemail,string ponumber,string accountid,string enquirytype,
                         string productid, string pricebookid,string purchaseid,string mobilephone){

        try{
            RestResponse res = RestContext.response;        
            //country code au or aus
            LeadFactory factory = new LeadFactory();
          
            Id leadId = factory.createLead(company,numberoflicences,term,confirmedunitPrice,totalamount,firstname,lastname,email,job,role,countrycode,postcode,productfamily,street1,street2,suburb,campaignid,accpayblefirstname,accpayblelastname,accpaybleEmail,ponumber,accountid, enquirytype, productid, pricebookid,purchaseid,mobilephone);
            statusModal = new ResponseModel();
            //statusModal = factory.status;
            System.debug('Anydatatype_msg' + leadId);
            if(leadId == null){
                res.statusCode = 400;
                res.responseBody = blob.valueOf(factory.leadException);
                system.debug('res.responseBody' + res.responseBody);
                //statusModal.statusCode = 400;
                
                return null;
            }else{
                Lead createdLead = [Select Id,LeadReferenceId__c from Lead where Id = :leadId];
                statusModal.objectID = createdLead.Id;
                //statusModal.referenceNumber = '47'+leadId;
                statusModal.referenceNumber = createdLead.LeadReferenceId__c;
               
            }
            return statusModal ;//TestUtility.getFakeId(Lead.SObjectType);
        }catch(Exception ex){
            system.debug('exception##'+ ex.getMessage());
            ExceptionLogData.InsertExceptionlog(null, LoggingLevel.ERROR, 'LeadService', ex, true, false, null, null);
            return null;
        }    
         
    }
}