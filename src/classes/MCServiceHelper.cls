/**
* @author: vishnu belkone
* @description: This class is to validate input parameters for Marketing cloud services
* @TesstClass: MCServiceHelperTest
*/
public with sharing class MCServiceHelper {
	public MCServiceHelper() {
		
	}

	public Static List<PricebookEntry> getPriceBookEntry(String productfamily,String accountCurrencyISO,Account account)
	{
		 
		List<Marketing_Cloud_Default_Pricebook__mdt> pricebookListForMC = [Select Pricebook__r.Name__c,Product_family__r.Name__c,Country__r.Code__c, 
                                                                                  Territory__r.Name__c  from Marketing_Cloud_Default_Pricebook__mdt
                                                                                  Where Product_family__r.Name__c = :productfamily and 
                                                                                  (Country__r.Code__c = :account.ShippingCountryCode OR Country__r.Code__c  = 'All') And                   
                                                                                  Territory__r.Name__c = :account.Territory__c];

                 System.debug('THis is to test' + pricebookListForMC);   
              Marketing_Cloud_Default_Pricebook__mdt priceBook = new Marketing_Cloud_Default_Pricebook__mdt();
              if(pricebookListForMC.size() > 1){
                  for(Marketing_Cloud_Default_Pricebook__mdt MCpriceBook : pricebookListForMC){
                    if(priceBook.Country__r.Code__c != 'All') {
                          priceBook = MCpriceBook;
                    }
                  }
              }else{
                priceBook = pricebookListForMC[0];
              }
		List<PricebookEntry> priceBookEntry;

		if(productfamily != null && priceBook.Pricebook__r.Name__c != null){
            priceBookEntry= [Select Id,Name, IsActive,Product2Id,PriceBook2Id from PriceBookEntry 
            					where 
            						product2.Family = :productfamily And 
            						Pricebook2.Name = :priceBook.Pricebook__r.Name__c and 
            						CurrencyIsoCode = :accountCurrencyISO
            						And  IsActive = true];
        }
        
		return priceBookEntry;           

	}

	public Static List<Product2> getProducts (Id productId)
	{
		List<Product2> productList;
		if(productId != null)
		{
			productList =[SELECT Id, Name, ACTNSPC__Quantity_Tier_2__c, ACTNSPC__Quantity_Tier_3__c, ACTNSPC__Quantity_Tier_4__c, 
	          ACTNSPC__Quantity_Tier_5__c, Quantity_Tier_6__c, Quantity_Tier_7__c, Quantity_Tier_8__c, Quantity_Tier_9__c, Quantity_Tier_10__c, 
	          Quantity_Tier_11__c, Quantity_Tier_12__c, Quantity_Tier_13__c, Quantity_Tier_14__c, Quantity_Tier_15__c, Quantity_Tier_16__c, 
	          Quantity_Tier_17__c 
	          FROM Product2 
	          where Id = :productId];
		}
		
		return productList;        
	}


}