/**
* @author: Baskar Venugopal
* @Description: This class will be called from Marketing cloud to create Opportunity, Quote,Quote PDF, line items and it will send auto Esign document thru Docverify API.
* @Test Class:  GenerateQuoteEsign_Test 
* @History: 
* 08/08/2017 created.
*/
public class GenerateQuoteAndEsignature {
    
    private static DocverifyEsignForQuotes__c quoteSettings=DocverifyEsignForQuotes__c.getValues('GenerateQuote');
    /**
* @author: Baskar Venugopal	
* @Description: This method will be called from Marketing cloud to create Opportunity, Quote,Quote PDF and line items.	 
* @History: 
* 08/08/2017 created.
*/
    public static Id createOpportunity(string prodFamily, integer term, Id accId,Id contactId,decimal quantity, string buyingIntent, string purchaseOrderNum,string requestType, string accPayableFirstName, string accPayableLastName, string accPayableEmail, decimal unitPrice,id campaignId,string purchaseId){
        Id newOppId;
        try{
            List<Account> accountList=[Select id,Name,School_Classification__c,Territory__c,Status__c,CurrencyIsoCode,OwnerId,owner.Email,Shared_Service_Center__c,ShippingCountryCode,CurrencyCopy__c,
                                       (SELECT AccountId,TeamMemberRole,UserId,User.Email FROM AccountTeamMembers),
                                       (SELECT  product2id,AccountId,Id,Status,Type_of_License__c,UsageEndDate FROM Assets 
                                        WHERE Type_of_License__c = 'Full Licence' AND  Status= 'Expired' and Product2.Family=: prodFamily)
                                       from Account where id =: accId];
            if((accountList !=null && accountList.size()>0  && accountList[0].School_Classification__c !=null) ){
                
                List<Contact_Roles__c> contactRolesList=[Select id,School_Territory__c,Job_Function__c from Contact_Roles__c 
                                                         where 	Account__c=:accId and Contact__c=:contactId and Role_Status__c='Current'];
                
                
                system.debug('accountList[0].CurrencyCopy__c##'+ accountList[0].CurrencyCopy__c);
                List<Asset> expLicences=accountList[0].Assets;
                string optyType=getOpportunityType( accountList[0].Status__c, expLicences);
                string schoolClassification = accountList[0].School_Classification__c;
                string recordType='Standard Opportunity';
                string currencyIsoCode = accountList[0].CurrencyIsoCode;
                string oppStage='Quote';
                string quoteStatus ='Draft'; 
                string territory=accountList[0].Territory__c;
                string jobFunction;
                string opportunityOwnerRole;
                string processType;
                boolean canSendEmailAlert= false;
                if(contactRolesList !=null && contactRolesList.size() >0 ){
                    jobFunction=contactRolesList[0].Job_Function__c;
                }
                
                Sales_Process__mdt salesMetaData= getTerritoryMetaData(jobFunction,territory, schoolClassification,requestType);
                if(salesMetaData !=null){
                    oppStage = salesMetaData.Opportunity_Stage__r.Name__c;
                    opportunityOwnerRole = salesMetaData.Account_Team_Role__r.name__c;
                    processType = salesMetaData.Process_Type__r.Name__c;
                    canSendEmailAlert = salesMetaData.Send_email__c;
                }
                
                List<PriceBookEntry> pbEntries= MCServiceHelper.getPriceBookEntry(prodFamily, currencyIsoCode,accountList[0]);
                if(pbEntries !=null && pbEntries.size() >0) {
                    Opportunity opportunity = OpportunityFactory.Create(optyType, recordType,pbEntries[0].priceBook2Id,prodFamily,accId, currencyIsoCode,oppStage); 
                    opportunity.CampaignId= campaignId;
                    opportunity.Marketing_Cloud_Purchase_Id__c= purchaseId;
                    opportunity.Is_Automatically_Created__c=true;
                    opportunity.Payment_Timing__c = 'Pay all up front';
                    opportunity.StageName =oppStage;

                    system.debug('Opportunity.StageName:'+ opportunity.StageName + ', requestType:' + requestType);

                    opportunity.Can_Send_Email_Alert__c =canSendEmailAlert;
                    if(requestType == 'Purchase'){
                        opportunity.Quote_Ignore__c = true;
                        if(oppStage =='Sold'){
                            opportunity.StageName ='Verbal Agreement';
                        }
                        
                        opportunity.Name = opportunity.Name + '-Purchase-' + system.Date.today().year() + system.Date.today().month() +system.Date.today().day();
                        opportunity.ReferenceText__c ='P-M-';
                    }else{
                        opportunity.Name = opportunity.Name +'-' + system.Date.today().year() + system.Date.today().month() +system.Date.today().day();
                        opportunity.ReferenceText__c ='Q-M-';
                    }
                    
                    if(term ==1){
                        opportunity.Auto_Renewal__c= true;
                        opportunity.Multi_Year_Deal__c= false;
                    }else if(term > 1){
                        opportunity.Auto_Renewal__c= false;
                        opportunity.Multi_Year_Deal__c= true;
                        opportunity.Number_of_years__c = String.valueOf(term);
                    }
                    
                    AccountTeamMember accTeamMember= getOpportunityOwner(accountList[0], opportunityOwnerRole);
                    Id newOppOwner= accountList[0].ownerId;
                    string KDMorBDMEmaiId= accountList[0].owner.Email;
                    if(accTeamMember != null){ 
                        newOppOwner =accTeamMember.UserId;
                        KDMorBDMEmaiId =accTeamMember.User.Email;
                    }
                    
                    opportunity.OwnerId= newOppOwner; 
                    opportunity.Buying_Objective__c=buyingIntent;
                    opportunity.Interest_Mathletics__c = true;
                    opportunity.Purchase_Order__c = purchaseOrderNum;
                    
                    //Set Opportunity Actian Currency
                    List<ACTNSCRM__Currency__c> currencyList = [Select Id from ACTNSCRM__Currency__c 
                                                                Where ACTNSCRM__ISO_Code__c =:accountList[0].CurrencyCopy__c ];
                    if(currencyList != null && currencyList.size() >0){
                        opportunity.ACTNSCRM__NS_Currency__c =currencyList[0].Id;
                    }
                    
                    insert opportunity;
                    
                    newOppId= opportunity.Id;
                    system.debug('opportunity.ACTNSCRM__NS_Currency__c##' + opportunity.ACTNSCRM__NS_Currency__c);
                    
                    if(requestType == 'Quote'){
                        if(processType == 'Fully-Automated' ){
                            quoteStatus= 'Approved';
                        }
                        system.debug('inside Quote Bronze##');
                        processQuoteRequest(pbEntries,opportunity.Id,contactId,quantity, term,quoteStatus,accountList[0].Territory__c, true,opportunity.Name, unitPrice,processType);
                    }else{
                        system.debug('inside purchase gold##');
                        processQuoteRequest(pbEntries,opportunity.Id,contactId,quantity,term,quoteStatus,accountList[0].Territory__c, false,opportunity.Name, unitPrice,processType); 
                    }
                    
                    createOpportunityContact(accId,opportunity.Id,contactId,accPayableFirstName,accPayableLastName,accPayableEmail,prodFamily, accountList[0].Shared_Service_Center__c);
                    
                    if(requestType == 'Purchase' && (processType =='Fully-Automated')){
                        opportunity.CloseDate = system.Date.today();
                        opportunity.Won_Lost_Reason__c ='Best product !';
                        opportunity.StageName ='Sold';
                        OpportunityTriggerDispatcher.resetTriggerFlags();
                        update opportunity;
                    }
                    createTask(opportunity,newOppOwner, accountList[0].Name,requestType,processType);
                }
            }
        }
        catch(Exception ex){
            system.debug('exception##'+ ex.getMessage());
            ExceptionLogData.InsertExceptionlog(newOppId, LoggingLevel.ERROR, 'GenerateQuoteAndEsignature', ex, true, false, null, null);
        }
        return newOppId;
    }
    
    private static Sales_Process__mdt getTerritoryMetaData(string jobFunction,string territory, string classification,string requestType){
        List<Sales_Process__mdt> salesMetadataList= [Select DeveloperName ,
                                                                   Account_Team_Role__r.name__c,Contact_Job_Function__r.Name__c,Name__c,Opportunity_Stage__r.Name__c,
                                                                   Process_Type__r.Name__c,School_Classification__r.Name__c,Territory__r.Name__c, Enquiry_Type__c,Send_email__c 
                                                                   from Sales_Process__mdt ];
        
        Sales_Process__mdt allMetadata;
        
        for(Sales_Process__mdt metaData : salesMetadataList){
            //system.debug('##'+ metaData.Sales_Process__r.Contact_Job_Function__r.Name__c +'##'+ metaData.Territory__c + '##'+ metaData.School_Classification__r.Name__c);
            if(jobFunction == metaData.Contact_Job_Function__r.Name__c && territory==metaData.Territory__r.Name__c && 
               classification == metaData.School_Classification__r.Name__c &&  requestType == metaData.Enquiry_Type__c){
                   return metaData;
               }
            if(metaData.Contact_Job_Function__r.Name__c == 'All' && territory==metaData.Territory__r.Name__c && 
               classification == metaData.School_Classification__r.Name__c &&  requestType == metaData.Enquiry_Type__c){
                   allMetadata= metaData;
               }
        }
        return allMetadata;
    }
    
    /**
* @author: Baskar Venugopal 
* @Description: Create a Task and assign it to KAM or BDM. If the school type is Bronze/Silver and request type is Purchase.
* @History: 
05/09/2017 created.
*/
    private static void createTask(Opportunity opportunity,id kdmOrBDMID, string schoolName, string requestType,string processType){
        string taskDesc='Activity type: Online Purchase \n'+ 'Activity Subtype: Purchase\n'+ 'Product: Mathletics\n'+
            'Comments:\n'+ schoolName + ' has requested to purchase Mathletics online. Please complete below tasks within 2 weeks:\n'+
            'Professional Training\n'+ 'Handover the relevant Client Manager';
        
        Task newTask=new Task();
        newTask.status = 'Not Started';
        newTask.Priority ='High';
        newTask.Product_Mathletics__c=true;
        newTask.whatId=opportunity.Id;
        newTask.Activity_Type__c='Online Purchase';
        
        if(requestType == 'Purchase' && processType =='Fully-Automated'){
            newTask.subject = 'New online purchase requires client onboarding';
            newTask.description = taskDesc;
            newTask.OwnerId=kdmOrBDMID;
            newTask.ActivityDate= system.date.today();
            newTask.Activity_Subtype__c='Purchase';
            insert newTask;
        }
        if(processType == 'Semi-Automated'){
            newTask.subject = 'New online purchase requires urgent review';
            if(opportunity.StageName == 'Quote'){
                taskDesc='Activity type: Online Purchase \n'+ 'Activity Subtype: Quote\n'+ 'Product: Mathletics\n'+
                    'Comments:\n'+ 'This Gold/Platinum school has requested for a quote to purchase Mathletics online.\n'+
                    'Please review and identify any up-sell or cross-sell opportunity.\n'+
                    'SLA: The opportunity contact must be contacted by COB NBD if not earlier.';
                newTask.OwnerId=opportunity.ownerId;
                newTask.Activity_Subtype__c= 'Quote';
            }
            if(requestType == 'Purchase' ){
                taskDesc='Activity type: Online Purchase \n'+ 'Activity Subtype: Purchase\n'+ 'Product: Mathletics\n'+
                    'Comments:\n'+ 'This Gold/Platinum school has requested to purchase Mathletics online.\n'+
                    'Please review and identify any up-sell or cross-sell opportunity.\n'+
                    'SLA: The opportunity contact must be contacted by COB NBD if not earlier.';
                newTask.OwnerId=kdmOrBDMID;
                newTask.Activity_Subtype__c= 'Purchase';
            }
            newTask.description = taskDesc;
            newTask.ActivityDate = system.date.today();
            newTask.IsReminderSet = true;
            newTask.ReminderDateTime = DateTime.newInstance(system.date.today().adddays(1), Time.newInstance(8, 0, 0, 0));
            insert newTask;
        }
        
    }
    
    /**
* @author: Baskar Venugopal	
* @Description: First priority is to find KDM then BDM. If both are not present then return Account's Owner Id.
* @History: 
24/08/2017 created.
*/
    private static AccountTeamMember getOpportunityOwner(Account account,string opportunityOwnerRole){
        AccountTeamMember teamMember;
        for(AccountTeamMember accTeamMember : account.AccountTeamMembers){
            if(accTeamMember.TeamMemberRole == opportunityOwnerRole){ 
                return accTeamMember ;
            }
        }
        return teamMember;
    } 
    
    /**
* @author: Baskar Venugopal	
* @Description: This method is used to get Opportunity type based on Account Status.
* @History: 
22/08/2017 created.
*/
    private static string getOpportunityType(string accStatus, List<Asset> licences){
        if(licences !=null && licences.size()>0 && accStatus == 'Lapsed Customer'){
            return 'Renewal';
        }
        else if(accStatus =='Potential Customer' || accStatus =='Former Customer'){
            // Create a Class for Lapsed Customer
            return 'New Business';
        }
        return 'Cross Sell';
    }
    
    /**
* @author: Baskar Venugopal	
* @Description: This method is used to create Opportunity Contact.
* @History: 
22/08/2017 created.
*/
    private static void createOpportunityContact(id accId, id opportunityId,id contactId, string accPayableFirstName, string accPayableLastName, string accPayableEmail,string prodFamily, id accSSCId){
        
        try{
            List<OpportunityContacts__c> opportunityContacts = new List<OpportunityContacts__c>();
            //To create Opportunity Contact
            OpportunityContacts__c  oppCont=OpportunityFactory.createOpportunityContact(accId,contactId,opportunityId,'Opportunity Contact');
            opportunityContacts.add(oppCont);
            
            ContactService.doPost(accId,contactId,'', '', 'Curriculum Coordinator','Opportunity Contact','',prodFamily,null);
            
            OpportunityContacts__c accPayable;
            Id newContactId;
            
            if(accSSCId != null &&string.isNotBlank(accPayableFirstName)  && string.isNotBlank(accPayableLastName)  && string.isNotBlank(accPayableEmail) ){
                newContactId =ContactService.doPost(accId,null,accPayableFirstName, accPayableLastName, 'Accounts/Finance','Accounts Payable',accPayableEmail,prodFamily,null);
                system.debug('newContactId##'+ newContactId);
            }
            else if(accSSCId == null &&string.isNotBlank(accPayableFirstName)  && string.isNotBlank(accPayableLastName)  && string.isNotBlank(accPayableEmail) ){
                newContactId =ContactService.doPost(accId,null,accPayableFirstName, accPayableLastName, 'Accounts/Finance','Accounts Payable',accPayableEmail,prodFamily, null);
                system.debug('newContactId##'+ newContactId);
                accPayable=OpportunityFactory.createOpportunityContact(accId,newContactId,opportunityId,'Accounts Payable');
                opportunityContacts.add(accPayable);
            }else if(accSSCId == null){
                List<Contact_Roles__c> contactRoles = [select Id,Role__c,Contact__c from Contact_Roles__c where
                                                       account__C =: accId and Role__c= 'Accounts Payable'  order by createdDate DESC limit 1] ;
                if(contactRoles == null || contactRoles.size() ==0){
                    accPayable=OpportunityFactory.createOpportunityContact(accId,contactId,opportunityId,'Accounts Payable');
                }else{
                    accPayable=OpportunityFactory.createOpportunityContact(accId,contactRoles[0].Contact__c,opportunityId,'Accounts Payable');
                }
                opportunityContacts.add(accPayable);
            }
            
            insert opportunityContacts;
            
        }
        catch(Exception ex){
            ExceptionLogData.InsertExceptionlog(opportunityId, LoggingLevel.ERROR, 'GenerateQuoteAndEsignature', ex, true, false, 'Method Name: createOpportunityContact', null);
        }
    }
    
    /**
* @author: Baskar Venugopal	
* @Description: This method is used to create Opportunity line item, Quote, Quote Line Item and email the  Quote PDF to Contact.
* @History: 
22/08/2017 created.
*/
    private static void processQuoteRequest( List<PriceBookEntry> pbEntries, id opportunityId,id contactId,decimal quantity, integer term,string quoteStatus,string accTerritory, boolean isQuote, string oppName,decimal unitPrice,string processType){
        
        try{
            date subscriptionStartDate = system.date.today();
            date subscriptionEndDate =subscriptionStartDate.addYears(term);
            subscriptionEndDate= subscriptionEndDate.addDays(-1);
            system.debug('isQuote##pbEntries'+ pbEntries);
            if(pbEntries != null && pbEntries.size() >0){
                OpportunityLineItem oppLineItem=OpportunityFactory.Create( quantity, true,subscriptionStartDate, subscriptionEndDate, quantity ,pbEntries[0].id );
                oppLineItem.OpportunityId =opportunityId;
                //oppLineItem.UnitPrice= unitPrice;
                oppLineItem.Price_Locked__c= true;
                insert oppLineItem; 
                system.debug('isQuote##'+ isQuote);
                if(isQuote){
                    Quote quote=OpportunityFactory.createQuote(oppName + '- Quote 1', opportunityId,pbEntries[0].priceBook2Id);
                    quote.Status=quoteStatus;
                    insert quote;
                    system.debug('quote##'+ quote);
                    //Opportunity Line item's Unit Price will have discount applied value.
                    oppLineItem = [Select id,UnitPrice from OpportunityLineItem where id=:oppLineItem.Id];
                    QuoteLineItem quoteLineItem= OpportunityFactory.createQuoteLineItem(pbEntries[0].Product2Id,quote.Id, pbEntries[0].id, quantity,oppLineItem.UnitPrice);
                    insert quoteLineItem;
                    
                    createQuotePDF(quote.Id,accTerritory);
                    
                    if(processType == 'Fully-Automated'){
                        callEsignBatch(quote.Id,contactId);
                    }
                    
                }
                
            }
        }
        catch(Exception ex){
            ExceptionLogData.InsertExceptionlog(opportunityId, LoggingLevel.ERROR, 'GenerateQuoteAndEsignature', ex, true, false, 'Method Name: processQuoteRequest', null);
        }
    }
    
    /**
* @author: Baskar Venugopal	
* @Description: This method is used to email the  Quote PDF to Contact. Need to schedule the Esign email after few minutes of Quote PDF generation so using 
* 				 Scheduler to schedule it after 5 minutes.	 
* @History: 
08/08/2017 created.
*/
    private static void callEsignBatch(id quoteId,id contactId){
        
        try{
            String day = string.valueOf(system.now().day());
            String month = string.valueOf(system.now().month());
            String hour = string.valueOf(system.now().hour());
            String minute ;
            if(system.now().minute() > 54){
                minute = string.valueOf(quoteSettings.Time_Delay_for_Esign__c);
                hour = string.valueOf(system.now().hour() +1);
            }
            else{
                minute = string.valueOf(system.now().minute() + quoteSettings.Time_Delay_for_Esign__c);
            }
            
            String second = string.valueOf(system.now().second());
            String year = string.valueOf(system.now().year());
            String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
            System.schedule('Esign Batch - ' + system.Datetime.now(), strSchedule, new SendEsignBatch(quoteId,contactId));
        }
        catch(Exception ex){
            ExceptionLogData.InsertExceptionlog(quoteId, LoggingLevel.ERROR, 'GenerateQuoteAndEsignature', ex, true, false, 'Method Name: callEsignBatch', null);
        }
    }
    
    /**
* @author: Baskar Venugopal	
* @Description: This method will create PDF for Quotes dynamically. Url hacking used to create PDF file.	 
* @History: 
08/08/2017 created.
*/
    @future(callout=true)
    public static void createQuotePDF(Id quoteId,string territory){
        //Id of quote Template based on the territory
        QuoteTemplatesByTerritory__c quoteTemplate=QuoteTemplatesByTerritory__c.getValues(territory);
        String templateID =quoteTemplate.TemplateId__c; 
        //This Url create the pdf for quote
        String quoteUrl = '/quote/quoteTemplateDataViewer.apexp?id=';
        quoteUrl +=quoteId;
        quoteUrl +='&headerHeight=190&footerHeight=188&summlid=';
        quoteUrl +=templateID ;
        quoteUrl +='#toolbar=1&navpanes=0&zoom=90';
        Blob pdfDoc;
        try{
            //Create pdf content
            PageReference quotePage = new PageReference(quoteUrl) ;
            //Document object of quote which hold the quote pdf
            QuoteDocument quotedoc = new QuoteDocument(); 
            //Get the content of Pdf.
            if(test.isRunningTest()){
                pdfDoc=Blob.valueOf('Unit Test file');
            }else{
                pdfDoc= quotePage.getContentAsPDF() ;
            }
            
            quotedoc.Document = pdfDoc;
            quotedoc.QuoteId = quoteId ;
            //quotedoc.Name = quoteId + '-'+ 'Quote Document-'+ system.date.today();
            insert quotedoc;
            system.debug('quotedoc##'+quotedoc);
        }
        catch(Exception ex){
            ExceptionLogData.InsertExceptionlog(quoteId, LoggingLevel.ERROR, 'GenerateQuoteAndEsignature', ex, true, false, 'Method Name: createQuotePDF + Future callout', null);
        }
    }
    
    /**
* @author: Baskar Venugopal	
* @Description: Calls Docverify API to send auto email for Esign. This method will be called from SendEsignBatch. 
* @History: 
08/08/2017 created.
*/
    public static void addNewDocAndEsign(QuoteDocument quoteDoc,id quoteId,id contactId){
        try{
            system.debug('quoteIds##'+quoteId + ' contactId##'+ contactId);
            Contact contact=[Select id, name,Email from Contact where id=: contactId];
            
            DocverifyAPI.DocVerify_x0020_Web_x0020_ServiceSoap docverify=new DocverifyAPI.DocVerify_x0020_Web_x0020_ServiceSoap();
            docverifyesign__DocVerify_Agreement__c eSign;
            
            //Retrieve Docverify API keys from custom settings.
            DocVerifyAPI__c apiKeys=DocVerifyAPI__c.getValues('DocVerifyAPI');
            string apikey= apiKeys.APIKey__c;
            string sigKey = apiKeys.Sigkey__c;
            
            DocverifyEsignForQuotes__c quoteSettings=DocverifyEsignForQuotes__c.getValues('GenerateQuote');
            
            String pdfFile = System.EncodingUtil.base64Encode(quoteDoc.Document);
            string emailTo= contact.Email;
            string emailCC =quoteSettings.Email_CC__c; 
            string message=quoteSettings.Esign_Message__c; 
            string docName=quoteDoc.Name;
            string descripton =quoteDoc.Quote.Description ;
            
            eSign=new docverifyesign__DocVerify_Agreement__c();
            if(!Test.isRunningTest()){
                //Call Docverify web service        
                string docVerifyId=docverify.AddNewDocumentESign(apikey,sigKey,pdfFile,1,docName,
                                                                 descripton,'',emailTo,message,false,false,
                                                                 '',false,false,'',false,'',
                                                                 false,'','',false,'',false,emailCC); 
                
                eSign.docverifyesign__Document_ID__c =docVerifyId;  
            }
            else{
                eSign.docverifyesign__Document_ID__c ='123';
            }
            eSign.docverifyesign__Quote__c = quoteId;
            eSign.Name=docName;
            eSign.docverifyesign__Status__c='Sent for Signature';
            insert esign;
            system.debug('esign##'+ esign);
        }
        catch(Exception ex){
            ExceptionLogData.InsertExceptionlog(quoteId, LoggingLevel.ERROR, 'GenerateQuoteAndEsignature', ex, true, false, 'Method Name: addNewDocAndEsign ', 'Parameters :QuoteDocument '+ quoteDoc + ' Quote id:'+ quoteId+ ' ContactId:'+ contactId);
        }
    }
}