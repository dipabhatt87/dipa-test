@RestResource(urlMapping='/school/price/*')
 global with sharing class SchoolPriceService {

    
    

    @httpGet
    global static void  Get(){
        try{
                RestResponse res = RestContext.response;
              if (res == null) {
                  res = new RestResponse();
                  RestContext.response = res;
              }

               //This is to get all request parameters in Map 
              Map<String, String> parameters =TerritoryPriceService.ToLowerCase( RestContext.request.params);

                
              string accountIdParam = parameters.get('accountid');       
              String numberoflicences =parameters.get('numberoflicences');
              string campaignIdParam =parameters.get('campaignid');
              String productfamily =parameters.get('productfamily');
              String terms =parameters.get('term');
              system.debug('parameters:' + parameters);
             
              //Validation--------------------------------          
              String responseBody = new MCServicesValidator().validateSchoolServiceInputParameters( accountIdParam, numberoflicences,  campaignIdParam,  productfamily,terms); 
              if(responseBody != null){
                  res.statusCode = 400;
                  res.responseBody=Blob.valueOf(responseBody);
                return;
              }       
              

              Id accountId = accountIdParam;
              //Id contactId = contactIdParam;        
              Id campaignId= campaignIdParam;

              PriceModel price =new PriceModel();
              double unitPrice=0;
            
            
              List<Account> accounts = [select Id,Name,School_Category__c,CurrencyIsoCode,Territory__c,School_Decile__c,ShippingCountryCode from account where Id = :accountid];
              system.debug('school Price service accounts:'+accounts[0]);
              if(new MCServicesValidator().checkIfAccountHasActiveLicence(accounts[0],productfamily) || new MCServicesValidator().checkIfAccountisNotFromCorrectTerritory(accounts[0])){
                  res.statusCode = 200;
                  price.ispriceavailable =     false;
                  res.responseBody=Blob.valueOf(JSON.serialize(price));
                  return;
              }

              if(accounts==null || accounts.size()==0)
              {
                   res.statusCode = 404;
                   res.responseBody=Blob.valueOf('Cannot find the requested Account');
                   return;
              }

              
              System.debug('This is to productfamily' + productfamily + 'THis is to productfamily' + accounts[0]);   
              List<Marketing_Cloud_Default_Pricebook__mdt> pricebookListForMC = [Select Pricebook__r.Name__c,Product_family__r.Name__c,Country__r.Code__c, 
                                                                                  Territory__r.Name__c ,
                                                                                   Country__r.Tax_Name__c from Marketing_Cloud_Default_Pricebook__mdt
                                                                                  Where Product_family__r.Name__c = :productfamily and 
                                                                                  (Country__r.Code__c = :accounts[0].ShippingCountryCode OR Country__r.Code__c  = 'All') And                   
                                                                                  Territory__r.Name__c = :accounts[0].Territory__c];

              System.debug('Opportunity_pricebookList For MC' + pricebookListForMC);   
              Marketing_Cloud_Default_Pricebook__mdt priceBook = new Marketing_Cloud_Default_Pricebook__mdt();
              if(pricebookListForMC.size() > 1){
                  for(Marketing_Cloud_Default_Pricebook__mdt MCpriceBook : pricebookListForMC){
                    if(MCpriceBook.Country__r.Code__c != 'All') {
                          priceBook = MCpriceBook;
                    }
                  }
              }else{
                priceBook = pricebookListForMC[0];
              }
              System.debug('THis is to tPricebook__rest' + priceBook.Pricebook__r.Name__c);  

              List<PricebookEntry> priceBookEntry = [Select Id,Name, IsActive,Product2Id,PriceBook2Id,Pricebook2.Name from PriceBookEntry 
                      where 
                        product2.Family = :productfamily And 
                        Pricebook2.Name = :priceBook.Pricebook__r.Name__c and 
                        CurrencyIsoCode = :accounts[0].CurrencyIsoCode
                        And  IsActive = true];
              
              System.debug('priceBookEntry' + priceBookEntry);  

                List<ACTNSPC__Price_Level_Entry__c> pricelevelEntries = [SELECT Id, ACTNSPC__Currency__c, ACTNSPC__Price_Level__c, ACTNSPC__External_Id__c,   ACTNSPC__Tier_1_Price__c, ACTNSPC__Tier_2_Price__c,ACTNSPC__Product__c  
                                          FROM ACTNSPC__Price_Level_Entry__c
                                          where ACTNSPC__Price_Level__R.Name = :priceBook.Pricebook__r.Name__c
                                          and ACTNSPC__Currency__R.Name      = :accounts[0].CurrencyIsoCode];
           

               System.debug('THis is to test' + priceLevelEntries);                                                         
              //End of Validation--------------------------------       

             
              List<Product2> productList;


              if(priceBookEntry!=null && priceBookEntry.size()>0){
                   productList = MCServiceHelper.getProducts(priceBookEntry[0].Product2Id) ;
              }
              
            //Function call to get tired price
            Decimal range=-1;
            if(accounts[0].School_Decile__c != null){
              range = Decimal.valueOf(accounts[0].School_Decile__c); 
            }else{
              range = 0;
            }

            String territory = accounts[0].Territory__c;
            String country = accounts[0].ShippingCountryCode;
             PriceCalculator priceCalculator = new PriceCalculator();
             System.debug('THis is to test' + priceBook.Pricebook__r.Name__c);
            priceCalculator.SetDiscountTiers(territory, country, priceBook.Pricebook__r.Name__c, productFamily);
             System.debug('THis is to PriceCalculator' + priceCalculator.discountTiers);
            boolean isPaymentUpfront =true;
            //decimal initialUnitPrice=50;
            AdvancedPriceModel advPriceModel = priceCalculator.GetPrice(priceLevelEntries[0].ACTNSPC__Tier_1_Price__c,  integer.valueOf(numberoflicences), integer.valueOf(terms), isPaymentUpfront,accounts[0]. School_Decile__c, accounts[0].School_Category__c);

                System.debug('This is to test advPriceModel' + advPriceModel);
         
            price.UnitPrice =  advPriceModel.UnitPriceIncludeTax;
            price.Tax       = advPriceModel.TaxAmount;
            price.currencyCode =accounts[0].CurrencyIsoCode ;
            price.Subtotal  = advPriceModel.FinalAmountExcludeTax ;
            price.TotalAmount  = advPriceModel.FinalAmountIncludeTax;
            price.DicountedPriceIncludedTax = advPriceModel.discountAmount;
            price.Taxname   = priceBook.Country__r.Tax_Name__c;
            price.ispriceavailable = true;
            price.pricebookid= priceBookEntry[0].Pricebook2id;
            price.productId= priceBookEntry[0].Product2Id;
            if(price.UnitPrice>0){
              
              price.ispriceavailable = true;
            }else{
              price.ispriceavailable = false;
            }

            res.responseBody = Blob.valueOf(JSON.serialize(price));
            res.statusCode = 200;      

        }catch(Exception ex){
            system.debug('exception##'+ ex.getMessage());
            ExceptionLogData.InsertExceptionlog(null, LoggingLevel.ERROR, 'SchoolPriceService', ex, true, false, null, null);
        }
    }   

    
}