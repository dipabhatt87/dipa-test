public with sharing class DuplicateContactDetailsCompController extends ComponentControllerBase{
    
    public LeadDuplicateContactHelper helper{get;set;}

    public string selectedcontactId{get; set;}
    //public string selectedAPcontactId{get; set;}
    //public Boolean sendEmailToOwner{get;set;}
    public Boolean createNewContact{get; set;}
    //public Boolean createNewAPContact{get; set;}
	 // public List<MatchingContact> matchingContactList{get;set;}
    //public List<MatchingContact> matchingContactListNextPrevious{get;set;}
   
    //public ContactCustomPagination paginator{get;set;}
    public Lead leadConvert{get;set;}
    public Boolean isAccountPayble {get;set;}
    //THis is set by the <apex:attribute> and is the lead to convert
    /*public Lead leadConvert {
        get; 
        set { 
                leadConvert = value;
            }
        }
    public Boolean isAccountPayble {
        get; 
        set { 
                isAccountPayble = value;
            }
        }*/
    public DuplicateContactDetailsCompController(){
      //leadConvert = value;
      //isAccountPayble = value;
      System.debug('Anydatatype_msg 1' + leadConvert);
      // getContacts();
       //paginator = new ContactCustomPagination(matchingContactList);
    }


    //This method is to return List of matching contact wrapper
    public List<MatchingContact> getContacts()
  {
    helper = new LeadDuplicateContactHelper();
    if(isAccountPayble == true){      
       return helper.getMathcingContacts(leadConvert.Account_Payable_First_Name__c,leadConvert.Account_Payable_Last_Name__c,leadConvert.Account_Payable_Email__c);  
    }else {        
        return  helper.getMathcingContacts(leadConvert.firstName,leadConvert.lastName,leadConvert.email);  
    }
    
  }



  public PageReference getSelected(){
    //If(isAccountPayble == true){
      selectedcontactId = ApexPages.currentPage().getParameters().get('contactId');
      createNewContact = false;      
   // }else{
    //  selectedcontactId = ApexPages.currentPage().getParameters().get('contactId');
    //   System.debug('This is selectedAPcontactId' + selectedAPcontactId);
   //     System.debug('This is selectedcontactId' + selectedcontactId);
   // }

    //System.debug('This is selectedAPcontactId' + selectedAPcontactId);
     //System.debug('This is selectedAPcontactId' + selectedAPcontactId);
    //createNewContact = false;
    return null;
  }
  

        /*public Boolean hasNext {
            get 
            {
               return null;// return paginator.hasNext();
            }
            set;
        }
        
        public Boolean hasPrevious {
            get 
            {
               return null;/// return paginator.hasPrevious();
            }
            set;
        }
        
        public void next() 
        {
            //matchingContactListNextPrevious = paginator.next();
        }
        
        public void previous() 
        {
           //matchingContactListNextPrevious = paginator.previous();
        }*/
  

}