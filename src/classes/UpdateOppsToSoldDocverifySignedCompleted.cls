/**
 * @author: Baskar Venugopal
 * @Description: This batch is used to send documents for ESign thru Docverify API.
 * @Test Class:  GenerateQuoteEsign_Test 
 * @History: 
    08/08/2017 created.
 */
public with sharing class UpdateOppsToSoldDocverifySignedCompleted implements StatelessBatchExecutor.CustomBatchable{   

     public void execute(List<Opportunity> scope){    
        updateOpportunities(scope);
    }
    
    public void finish(){}

    private void updateOpportunities(List<Opportunity> opportunityList){
        try{
            DocverifyEsignForQuotes__c quoteSettings=DocverifyEsignForQuotes__c.getValues('GenerateQuote');
            for(Opportunity oppTobeUpdated : opportunityList){
                oppTobeUpdated.Is_Docverify_Signed_Or_Completed__c= false;
                oppTobeUpdated.StageName =quoteSettings.OppStage_Doc_Signed__c;
                oppTobeUpdated.Won_Lost_Reason__c='Best product !';
                 system.debug('oppTobeUpdated##'+ oppTobeUpdated);
            }
                system.debug('oppTobeUpdatedList'+ opportunityList);
            update opportunityList;     
        }catch(Exception ex){
            system.debug('exception##'+ ex.getMessage());
            ExceptionLogData.InsertExceptionlog(null, LoggingLevel.ERROR, 'UpdateOppsToSoldDocverifySignedCompleted', ex, true, false, null, null);
        }
        
    }
}