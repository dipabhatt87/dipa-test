/**
 * @author: Baskar Venugopal
 * @Description: Test class for GenerateQuoteAndEsignature and sendEsignBatch.
 * @Test Class:  GenerateQuoteEsign_Test 
 * @History: 
	08/08/2017 created.
 */
@isTest
public class GenerateQuoteEsign_Test {

    @testSetup static void setup()
    {
        FakeObjectFactory.testclasssetup();
        String productFamily = 'Mathletics'; 
        DocverifyEsignForQuotes__c  quotesSetting=new DocverifyEsignForQuotes__c();
        quotesSetting.name='GenerateQuote';
		quotesSetting.Esign_Message__c='test message';
        quotesSetting.OppStage_Doc_Signed__c='Sold';
        quotesSetting.Time_Delay_for_Esign__c=5;
        quotesSetting.Marketing_User_Id__c =UserInfo.getUserId();
        quotesSetting.Opp_Stage_Doc_Declined__c='Lost';
        quotesSetting.Quote_Template__c='0EH28000001UV6m';
        insert quotesSetting;
        
        QuoteTemplatesByTerritory__c template=new QuoteTemplatesByTerritory__c();
        template.Name='APAC';
        template.TemplateId__c='0EH28000001UV6m';
        insert template;
        
        // PricebooksForNewPriceStructure__c pbNewPrice=new PricebooksForNewPriceStructure__c();
        // pbNewPrice.Name = productFamily;
        // pbNewPrice.Pricebook__c ='*Subsidised/Special Rate';
        // insert pbNewPrice;
        
        DocVerifyAPI__c docVerifyAPI=new DocVerifyAPI__c();
        docVerifyAPI.name='DocVerifyAPI';
        docVerifyAPI.APIKey__c='1234';
        docVerifyAPI.SigKey__c='xyz'; 
        insert docVerifyAPI;
        
        Product2 product = FakeObjectFactory.GetProduct(productFamily);
        insert product;
        
		PricebookEntry pricebookEntry = FakeObjectFactory.GetPriceBook(product, 10);   
        insert pricebookEntry;   
        
        List<Pricebook__mdt> pricebooks = [select Id, name__c from Pricebook__mdt where name__c like '%decile%'];

 		Pricebook2 customPB = new Pricebook2(Name=pricebooks[0].Name__c, isActive=true);
        insert customPB;
        
         PricebookEntry customPrice = new PricebookEntry(
         Pricebook2Id = customPB.Id, Product2Id = product.Id,
         UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
    }
    
    private static void createContactRole(id accId,id contId, string jobFunction){
        Contact_Roles__c contactRole=new Contact_Roles__c();
		contactRole.Account__c = accId;
        contactRole.Contact__c =contId;
        contactRole.School_Territory__c ='APAC';
        contactRole.Job_Function__c =jobFunction;
        contactRole.Role_Status__c='Current';
        contactRole.MInfluencer__c=true;
        insert contactRole;
    }
    
    @isTest static void schoolPurchaseServiceTest_bronzeSchool(){
        Trigger_Handler__c triggerHandler = FakeObjectFactory.GetTriggerHandler();
        insert triggerHandler;
        
		String productFamily = 'Mathletics';    
        FakeObjectFactory.CreateFakeCurrency('AUD');
        
        
        Account bronzeAccount  = FakeObjectFactory.GetSchoolAccount();
        bronzeAccount.School_Classification__c='Bronze';
        bronzeAccount.CurrencyIsoCode = 'AUD';
        bronzeAccount.ShippingCountry='Australia';
        bronzeAccount.Territory__c='APAC';
        insert bronzeAccount; 
        
        Contact contact=FakeObjectFactory.GetContact();
        TestUtility.AssignAccountToContact(BronzeAccount, contact);    
        insert contact;
        
        createContactRole(bronzeAccount.Id,contact.Id,'Admin');
        
        AccountTeamMember KAM=new AccountTeamMember();
        KAM.AccountId=bronzeAccount.Id;
        KAM.UserId =UserInfo.getUserId();
        KAM.TeamMemberRole ='KAM';
        insert KAM;
        
        Campaign  campaign        			= new Campaign ();
        campaign.Name               		= 'test campaign';
        campaign.IsActive   				= true;
        campaign.Status    					= 'campaignName';
        campaign.Type      					= 'campaignName';
        campaign.Discount__c        		= 25;
        campaign.Job_Code__c        		= '1234567890';
        campaign.Product_Family__c			= 'Mathletics';
		campaign.School_Type__c				= 'School';
		campaign.Job_Code_Internal_id__c 	= '123456';
        
        insert campaign;
        
        
        
        test.startTest();

        ResponseModel respModel =SchoolPurchaseService.ConfirmPurchase(bronzeAccount.id, contact.id, productFamily, campaign.id, 10, 1, 1, 10, 'first','last', 'test@test.com', '5', 'Quote', '', '', '');
		system.debug('respModel##'+ respModel);
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI ='/school/purchase';
        req.httpMethod = 'POST';
        RestContext.request = req;
    	RestContext.response= res;
        
        //Account Id Null
        respModel = SchoolPurchaseService.ConfirmPurchase(null, contact.id, productFamily, campaign.id, 10, 1, 1, 10, 'first','last', 'test@test.com', '5', 'Quote', '', '', '');
        System.assertEquals(res.statusCode, 400);
        
        //ContactId Null
        respModel = SchoolPurchaseService.ConfirmPurchase(bronzeAccount.id, null, productFamily, campaign.id, 10, 1, 1, 10, 'first','last', 'test@test.com', '5', 'Quote', '', '', '');
        System.assertEquals(res.statusCode, 400);
        
        //Number of licence Null
        respModel = SchoolPurchaseService.ConfirmPurchase(bronzeAccount.id, contact.id, productFamily, campaign.id, 0, 1, 1, 10, 'first','last', 'test@test.com', '5', 'Quote', '', '', '');
        System.assertEquals(res.statusCode, 400);
        
        //Product Famil is Null
        respModel = SchoolPurchaseService.ConfirmPurchase(bronzeAccount.id, contact.id, '', campaign.id, 10, 1, 1, 10, 'first','last', 'test@test.com', '5', 'Quote', '', '', '');
        System.assertEquals(res.statusCode, 400);
        
        //Unit Price is  0
        respModel = SchoolPurchaseService.ConfirmPurchase(bronzeAccount.id, contact.id, productFamily, campaign.id, 10, 1, 0, 10, 'first','last', 'test@test.com', '5', 'Quote', '', '', '');
        System.assertEquals(res.statusCode, 400);
        
        //Total Amount  is 0
        respModel = SchoolPurchaseService.ConfirmPurchase(bronzeAccount.id, contact.id, productFamily, campaign.id, 10, 1, 1, 0, 'first','last', 'test@test.com', '5', 'Quote', '', '', '');
        System.assertEquals(res.statusCode, 400);
        
        //Enquiry Type  is Null
        respModel = SchoolPurchaseService.ConfirmPurchase(bronzeAccount.id, contact.id, productFamily, campaign.id, 10, 1, 1, 10, 'first','last', 'test@test.com', '5', null, '', '', '');
        System.assertEquals(res.statusCode, 400);
        
        //Enquiry Type  is Not Quote or Purchase
        respModel = SchoolPurchaseService.ConfirmPurchase(bronzeAccount.id, contact.id, productFamily, campaign.id, 10, 1, 1, 10, 'first','last', 'test@test.com', '5', 'abc', '', '', '');
        System.assertEquals(res.statusCode, 400);

        
        respModel = SchoolPurchaseService.ConfirmPurchase(bronzeAccount.id, contact.id, productFamily, campaign.id, 10, 1, 1, 10, 'first','last', 'test@test.com', '5', 'Quote', '', '', '');
        Id bronzeOppId =respModel.objectID;
        system.assertNotEquals(bronzeOppId, null);
        
        List<Opportunity> newOpportunity=[Select id,name from Opportunity where id=:bronzeOppId];
        
        system.assertEquals('Mathletics' + ' - ' + 'New Business'  +'-' + system.Date.today().year() + system.Date.today().month() +system.Date.today().day(), newOpportunity[0].Name);
		
        test.stopTest();
    }
    
    @isTest static void bronzeSchool_With_CampaignDiscount_OpportunityShouldbeCreated(){
        Trigger_Handler__c triggerHandler = FakeObjectFactory.GetTriggerHandler();
        insert triggerHandler;
        
		String productFamily = 'Mathletics';    
        FakeObjectFactory.CreateFakeCurrency('AUD');
        
        
        Account bronzeAccount  = FakeObjectFactory.GetSchoolAccount();
        bronzeAccount.School_Classification__c='Bronze';
        bronzeAccount.CurrencyIsoCode = 'AUD';
        bronzeAccount.ShippingCountry='Australia';
        bronzeAccount.Territory__c='APAC';
        insert bronzeAccount; 
        
        Contact contact=FakeObjectFactory.GetContact();
        TestUtility.AssignAccountToContact(BronzeAccount, contact);    
        insert contact;
        
        AccountTeamMember KAM=new AccountTeamMember();
        KAM.AccountId=bronzeAccount.Id;
        KAM.UserId =UserInfo.getUserId();
        KAM.TeamMemberRole ='KAM';
        insert KAM;
        
        Campaign  campaign        			= new Campaign ();
        campaign.Name               		= 'test campaign';
        campaign.IsActive   				= true;
        campaign.Status    					= 'campaignName';
        campaign.Type      					= 'campaignName';
        campaign.Discount__c        		= 25;
        campaign.Job_Code__c        		= '1234567890';
        campaign.Product_Family__c			= 'Mathletics';
		campaign.School_Type__c				= 'School';
		campaign.Job_Code_Internal_id__c 	= '123456';
        
        insert campaign;
        
        test.startTest();
        Id bronzeOppId = GenerateQuoteAndEsignature.createOpportunity('Mathletics',1,bronzeAccount.Id,contact.Id,10,'test', '1123','Quote','first','last','test@test.com',5,campaign.id,null);
        system.assertNotEquals(bronzeOppId, null);
        
        List<Opportunity> newOpportunity=[Select id,name from Opportunity where id=:bronzeOppId];
        
        system.assertEquals('Mathletics' + ' - ' + 'New Business'  +'-' + system.Date.today().year() + system.Date.today().month() +system.Date.today().day(), newOpportunity[0].Name);

        test.stopTest();
    }

    @isTest static void goldSchool_NoCampaignDiscount_OpportunityShouldbeCreated(){

        Account goldAccount  = FakeObjectFactory.GetSchoolAccount();
        goldAccount.School_Classification__c='Gold';
        goldAccount.CurrencyIsoCode = 'AUD';
        goldAccount.ShippingCountry='Australia';
        goldAccount.Territory__c='APAC';
        insert goldAccount; 
        
        Contact contact=FakeObjectFactory.GetContact();
        TestUtility.AssignAccountToContact(goldAccount, contact);    
        insert contact;
		createContactRole(goldAccount.Id,contact.Id,'Admin');
        Id goldOppId=GenerateQuoteAndEsignature.createOpportunity('Mathletics',3,goldAccount.Id,contact.Id,10,'test', '1123','Purchase','','','',7,null,null);
        system.assertNotEquals(goldOppId, null);
    }

    @isTest static void silverSchool_NoCampaignDiscount_OpportunityShouldbeCreated(){

        Account silverAccountPurchase  = FakeObjectFactory.GetSchoolAccount();
        silverAccountPurchase.School_Classification__c='Silver';
        silverAccountPurchase.CurrencyIsoCode = 'AUD';
        silverAccountPurchase.ShippingCountry='Australia';
        silverAccountPurchase.Territory__c='APAC';
        insert silverAccountPurchase; 
        
        Contact contact=FakeObjectFactory.GetContact();
        TestUtility.AssignAccountToContact(silverAccountPurchase, contact);    
        insert contact;
        Id silverOppId=GenerateQuoteAndEsignature.createOpportunity('Mathletics',3,silverAccountPurchase.Id,contact.Id,10,'test', '1123','Purchase','','','',7,null,null);
        system.assertNotEquals(silverOppId, null);
    }
    
     @isTest static void docverify_BronzeSchool_NoCampaignDiscount_SendQuoteDocument(){
         
        Account bronzeAccount  = FakeObjectFactory.GetSchoolAccount();
        bronzeAccount.School_Classification__c='Bronze';
        bronzeAccount.CurrencyIsoCode = 'AUD';
        bronzeAccount.ShippingCountry='Australia';
        bronzeAccount.Territory__c='APAC';
        insert bronzeAccount; 
        
        Contact contact=FakeObjectFactory.GetContact();
        TestUtility.AssignAccountToContact(BronzeAccount, contact);    
        insert contact;
            
        Id bronzeOppId = GenerateQuoteAndEsignature.createOpportunity('Mathletics',1,bronzeAccount.Id,contact.Id,10,'test', '1123','Quote','first','last','test@test.com',5,null,null);
        List<Quote> testQuotes=[Select id from Quote limit 1];
        Datetime dt = Datetime.now().addMinutes(1);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        test.startTest();
        System.schedule('Esign Batch - Test' , CRON_EXP, new SendEsignBatch(testQuotes[0].id,contact.Id));
		test.stopTest();
        List<docverifyesign__DocVerify_Agreement__c> newDocVerifyDoc=[Select id,docverifyesign__Status__c from docverifyesign__DocVerify_Agreement__c where docverifyesign__Quote__c =: testQuotes[0].id];
        //system.assertEquals('Sent for Signature', newDocVerifyDoc[0].docverifyesign__Status__c);

     } 
    
     @isTest static void docverifyWebHook_Signed_ExecuteBatch_Update_Opportunity_To_Sold(){
        Account bronzeAccount  = FakeObjectFactory.GetSchoolAccount();
        bronzeAccount.School_Classification__c='Bronze';
        bronzeAccount.CurrencyIsoCode = 'AUD';
        bronzeAccount.ShippingCountry='Australia';
        bronzeAccount.Territory__c='APAC';
        insert bronzeAccount; 
        
        Contact contact=FakeObjectFactory.GetContact();
        TestUtility.AssignAccountToContact(BronzeAccount, contact);    
        insert contact;
            
        Id bronzeOppId = GenerateQuoteAndEsignature.createOpportunity('Mathletics',1,bronzeAccount.Id,contact.Id,10,'test', '1123','Quote','first','last','test@test.com',5,null,null);
        List<Quote> testQuotes=[Select id from Quote limit 1];
        
         if(testQuotes != null && testQuotes.size() >0){
            docverifyesign__DocVerify_Agreement__c eSign=new docverifyesign__DocVerify_Agreement__c();
        	esign.docverifyesign__Quote__c =testQuotes[0].id;
        	eSign.docverifyesign__Document_ID__c ='1234';
        	eSign.Name='docName';
        	eSign.docverifyesign__Status__c='viewed';
        	insert eSign;
         }
       
        
        PageReference webhook = Page.DocVerify_WebHook;
        Test.setCurrentPage(webhook);
        DocVerifyWebhook_Controller controller=new DocVerifyWebhook_Controller();
        
        ApexPages.currentPage().getParameters().put('docId', '1234');
		ApexPages.currentPage().getParameters().put('signer', 'xyz');
        ApexPages.currentPage().getParameters().put('status', 'signed');
        ApexPages.currentPage().getParameters().put('statusdate', string.valueOf(system.date.today()));
        
        controller.updateStatus();
        string  params=controller.parameterDetails;
        test.startTest();
        UpdateOppsToSoldFromDocverifySchedular schedular = new UpdateOppsToSoldFromDocverifySchedular();
		String sch = '0 0 23 * * ?';
		system.schedule('Test Opportunity Update schedular', sch, schedular);
        test.stopTest();
    }

}