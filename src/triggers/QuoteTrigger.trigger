trigger QuoteTrigger on Quote (before insert, before update) {
     TriggerFactory.createTriggerDispatcher(Quote.sObjectType);
}