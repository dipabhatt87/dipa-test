trigger ContactTrigger on Contact (after insert, after update, before insert, before update) {
 // This is the only line of code that is required.
 if(!Trigger_Handler__c.getInstance('Block Trigger').Block_Trigger_Execution__c) {
	TriggerFactory.createTriggerDispatcher(Contact.sObjectType);
  }
}